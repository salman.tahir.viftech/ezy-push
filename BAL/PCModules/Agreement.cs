﻿using DAL;
using Microsoft.Store.PartnerCenter;
using Microsoft.Store.PartnerCenter.Models.Agreements;
using Partner_Center.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.PCModules
{
    public class Agreements : AADAuthentication
    {
        public static IAggregatePartner partnerOperation = null;

        public Agreements(string UserName,string Password)
            :base(UserName,Password)
        {
            partnerOperation = this.UserPartnerOperation;
        }
        public Agreement CreateAgreement(MicrosoftAgreement _agreement)
        {
            var agreement = new Agreement
            {
                UserId = _agreement.UserMCID,
                DateAgreed = DateTime.UtcNow,
                Type = AgreementType.MicrosoftCloudAgreement,
                TemplateId = _agreement.TempleteMCID,
                PrimaryContact = new Contact
                {
                    FirstName = _agreement.FirstName,
                    LastName = _agreement.LastName,
                    Email = _agreement.Email,
                    PhoneNumber = _agreement.Phone
                }
            };

            return partnerOperation.Customers.ById(_agreement.CustomerMCID).Agreements.Create(agreement);
        }
    }
}
