﻿using BAL.Repositories;
using DAL.DBEntities;
using EZY_Project.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EZY_Project.Controllers
{
    public class InvoiceController : Controller
    {
        // GET: Invoice
        public ActionResult Index()
        {
            return View();
        }
        public void DownloadInvoice()
        {
            string viewPrint = "View";
            int CustomerId = Convert.ToInt32(Request.RawUrl.Split('/')[3].Split('-')[0]);
            //int month=  Convert.ToInt32(Request.RawUrl.Split('/')[3].Split('-')[1]);
            //int year = Convert.ToInt32(Request.RawUrl.Split('/')[3].Split('-')[2]);
            //string Type= Request.RawUrl.Split('/')[3].Split('-')[3];


            InvoiceStructure model = new CustomerRepository().GetCustomerById(CustomerId);
            string dataa = PartialView(viewPrint, model).RenderToString(this.ControllerContext);
            //dataa = dataa.Replace("/Content", folderPath);


            // vt_Common.RenderPartialToString(this, "PrintViewData", ModelData, VDD, TDD);
            StringReader sr = new StringReader(dataa);//Request.Form[hfGridHtml.UniqueID]
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 20f, 20f);
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
            pdfDoc.Close();
            //string file = HostingEnvironment.MapPath("~/Template/clientregularization.pdf");
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename= Certificate.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            Response.Write(pdfDoc);
            Response.OutputStream.Flush();
            Response.End();
        }
    }
}