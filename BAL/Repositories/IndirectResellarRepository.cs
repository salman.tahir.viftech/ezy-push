﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repositories
{
    public class IndirectResellarRepository : BaseRepository
    {

        public IndirectResellarRepository()
            : base()
        { }

        public IndirectResellarRepository(OMSEntities ContextDB)
            : base()
        {
            DBContext = ContextDB;
        }

        public IList<vt_Customers> GetCustomers(int id)
        {
            //SeekBasedResourceCollection<Microsoft.Store.PartnerCenter.Models.Customers.Customer> Customer = new CustomerModule().GetCustomers();
            //foreach (var item in Customer)
            //{

            //}
            return DBContext.ExclueAll().vt_Customers.Where(x => x.UserId == id && x.IsdirectResellar == true).ToList();
        }

        public IList<vt_User> GetUsers()
        {
            return DBContext.ExclueAll().vt_User.ToList();
        }
    }
}
