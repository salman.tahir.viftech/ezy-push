﻿using BAL.Repositories;
//using CsvHelper;
using DAL.DBEntities;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EZY_Project.Controllers
{
    public class uploadfile
    {
        public string Type { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
    }
    public class FileUploadController : BaseController
    {
        // GET: FileUpload
        FileUploadRepository fileuploadrepo;
        public FileUploadController()
        {
            fileuploadrepo = new FileUploadRepository(new OMSEntities());
        }
        public ActionResult Index()
        {
            //ViewBag.Year = fileuploadrepo.GetYearMonth().Select(x=>x.OrderDate.ToString("YYYY"));
            var year = (from c in fileuploadrepo.GetYearMonth()
                        select new { Year = c.OrderDate.Value.ToString("yyyy"), Month = c.OrderDate.Value.ToString("MM"), MonthName = c.OrderDate.Value.ToString("MMM") }).ToList();
            ViewBag.Year = year.Select(x => new { Year = x.Year }).Distinct().ToList();
            ViewBag.Month = year.Select(x => new { Month = x.Month, MonthName = x.MonthName }).Distinct().ToList();

            //ViewBag.Month =;
            return View();
        }
        [HttpPost]
        public ActionResult GetFilesData()
        {

            var allcustomer = fileuploadrepo.GetInvoivesData();
            var abc = JsonConvert.SerializeObject(allcustomer, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            var result = Json(new { data = abc }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [HttpPost]
        public ActionResult upload(uploadfile ProductName, HttpPostedFileBase file)
        {
            bool isSavedSuccessfully = false;

            foreach (string fileName in Request.Files)
            {
                //HttpPostedFileBase file = Request.Files[fileName];
                if (System.IO.Path.GetExtension(file.FileName) == ".xlsx" || System.IO.Path.GetExtension(file.FileName) == ".xls")
                {
                    string _FileName = System.IO.Path.GetFileName(file.FileName);
                    string _path = System.IO.Path.Combine(Server.MapPath("~/UploadedFiles"), DateTime.Now.ToString("dd-MM-yyyy") + System.IO.Path.GetExtension(file.FileName));
                    file.SaveAs(_path);
                    DataTable dt = ConvertExcelToDataTable(_path, true);
                    //if (!_FileName.Contains("EZYTechnologiesPvt.Ltd."))
                    //{
                    var dValue = from row in dt.AsEnumerable()
                                 where row.Field<string>("Regions") == "PK"
                                 select row;
                    foreach (var item in dValue)
                    {
                        if (fileName.Contains("Software-Subscriptions"))
                        {
                            GetByProductID(item["ProductId"].ToString(), item["SKUId"].ToString(), item["ListPrice"].ToString(), item["Msrp"].ToString());
                        }
                        else if (fileName.Contains("Cloud-Reseller-Pricelist"))
                        {
                            GetByOfferID(item["Offer ID"].ToString(), item["List Price"].ToString(), item["ERP Price"].ToString());
                        }
                    }
                    //}

                }
                else if (System.IO.Path.GetExtension(file.FileName) == ".csv")
                {
                    string _FileName = System.IO.Path.GetFileName(file.FileName);
                    string _path = System.IO.Path.Combine(Server.MapPath("~/UploadedFiles"), DateTime.Now.ToString("dd-MM-yyyy") + System.IO.Path.GetExtension(file.FileName));
                    file.SaveAs(_path);
                    //readCsv(_path);
                    SqlBulk(_path, ProductName.Type,ProductName);
                }

                //You can Save the file content here

                isSavedSuccessfully = true;
            }

            return View("Index");
        }

        public static DataTable ConvertExcelToDataTable(string path, bool hasHeader)
        {
            try
            {
                DataSet ds = new DataSet();
                using (var pck = new ExcelPackage())
                {
                    using (var stream = System.IO.File.OpenRead(path))
                    {
                        pck.Load(stream);
                    }
                    ExcelWorksheets sheets = pck.Workbook.Worksheets;
                    try
                    {


                        //for (int s = 1; s <= sheets.Count; s++)
                        {
                            var ws = sheets[1];
                            DataTable tbl = new DataTable();
                            foreach (var firstRowCell in ws.AutoFilterAddress != null ? ws.Cells[ws.AutoFilterAddress.Start.Row, ws.Dimension.Start.Column, ws.AutoFilterAddress.Start.Row, ws.Dimension.End.Column] : ws.Cells[ws.SelectedRange.Start.Row, ws.Dimension.Start.Column, ws.SelectedRange.Start.Row, ws.Dimension.End.Column])
                            //foreach (var firstRowCell in ws.Cells[ws.Dimension.Start.Row, ws.Dimension.Start.Column, ws.Dimension.Start.Row, ws.Dimension.End.Column])
                            {
                                if (!tbl.Columns.Contains(firstRowCell.Text))
                                {
                                    tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                                }
                                else
                                {
                                    return ds.Tables[0];
                                }
                            }
                            var startRow = hasHeader ? ws.AutoFilterAddress == null ? ws.SelectedRange.Start.Row + 1 : ws.AutoFilterAddress.Start.Row + 1 : 1;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Text;
                                }
                            }
                            //      var emptyRows =
                            //tbl.Select()
                            //    .Where(
                            //        row =>
                            //            tbl.Columns.Cast<DataColumn>()
                            //                .All(column => string.IsNullOrEmpty(row[column].ToString()))).ToArray();
                            //      Array.ForEach(emptyRows, x => x.Delete());

                            //      var emptyColumns =
                            //          tbl.Columns.Cast<DataColumn>()
                            //              .Where(column => tbl.Select().All(row => string.IsNullOrEmpty(row[column].ToString())))
                            //              .ToArray();
                            //      Array.ForEach(emptyColumns, column => tbl.Columns.Remove(column));
                            //      tbl.AcceptChanges();

                            tbl.TableName = ws.Name;
                            ds.Tables.Add(tbl);
                        }
                    }
                    catch (Exception ex)
                    {
                        ds.Tables[0].TableName = "Error";
                        return ds.Tables[0];
                    }

                    return ds.Tables[0];

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //int i = 0;

        }
        public static string ConnectionString = new OMSEntities().Database.Connection.ConnectionString.ToString();//System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
        public static void GetByProductID(string ProductID, string SKUID, string List_Price, string MSRP)
        {
            //var con = "Data Source=DESKTOP-GVQ6T7E;Initial Catalog=OMS;Connection Timeout=1000;user id=sa;password=viftech;";

            using (SqlConnection myConnection = new SqlConnection(ConnectionString))
            {
                string oString = "Select * from vt_ProductDetails where MCID=@MCID  and skuId=@skuId";
                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                oCmd.Parameters.AddWithValue("@MCID", ProductID);
                oCmd.Parameters.AddWithValue("@skuId", SKUID);
                myConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {

                        string Updatequery = "Update vt_ProductDetails set ListPrice='" + List_Price + "',Msrp='" + MSRP + "' where MCID=@MCID  and skuId=@skuId";
                        //          System.Data.SqlClient.SqlConnection sqlConnection1 =
                        //new System.Data.SqlClient.SqlConnection("Data Source=DESKTOP-GVQ6T7E;Initial Catalog=OMS;Connection Timeout=1000;user id=sa;password=viftech;");

                        System.Data.SqlClient.SqlConnection sqlConnection1 =
             new System.Data.SqlClient.SqlConnection(ConnectionString);

                        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddWithValue("@MCID", ProductID);
                        cmd.Parameters.AddWithValue("@skuId", SKUID);
                        cmd.CommandText = Updatequery;
                        cmd.Connection = sqlConnection1;

                        sqlConnection1.Open();
                        cmd.ExecuteNonQuery();
                        sqlConnection1.Close();
                        //UpdatePrice(ProductID, List_Price, MSRP);
                        //matchingPerson.firstName = oReader["FirstName"].ToString();
                        //matchingPerson.lastName = oReader["LastName"].ToString();


                    }

                    myConnection.Close();
                }
            }
        }

        public static void GetByOfferID(string OfferID, string List_Price, string ERP_Price)
        {
            //var con = "Data Source=DESKTOP-GVQ6T7E;Initial Catalog=OMS;Connection Timeout=1000;user id=sa;password=viftech;";

            using (SqlConnection myConnection = new SqlConnection(ConnectionString))
            {
                string oString = "Select * from vt_Products where MCOfferID=@MCOfferID";
                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                oCmd.Parameters.AddWithValue("@MCOfferID", OfferID);
                myConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        UpdatePrice(OfferID, List_Price, ERP_Price);
                        //matchingPerson.firstName = oReader["FirstName"].ToString();
                        //matchingPerson.lastName = oReader["LastName"].ToString();


                    }

                    myConnection.Close();
                }
            }
        }

        public static void UpdatePrice(string OfferID, string List_Price, string ERP_Price)
        {
            string Updatequery = "Update vt_Products set List_Price='" + List_Price + "',ERP_Price='" + ERP_Price + "' where MCOfferID='" + OfferID + "'";
            //          System.Data.SqlClient.SqlConnection sqlConnection1 =
            //new System.Data.SqlClient.SqlConnection("Data Source=DESKTOP-GVQ6T7E;Initial Catalog=OMS;Connection Timeout=1000;user id=sa;password=viftech;");

            System.Data.SqlClient.SqlConnection sqlConnection1 =
 new System.Data.SqlClient.SqlConnection(ConnectionString);

            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = Updatequery;
            cmd.Connection = sqlConnection1;

            sqlConnection1.Open();
            cmd.ExecuteNonQuery();
            sqlConnection1.Close();
        }

        public static void readCsv(string path)
        {
            List<string> result = new List<string>();
            DataTable dt = new DataTable();
            string value;
            int j = 0;
            using (TextReader fileReader = System.IO.File.OpenText(path))
            {
                var csv = new CsvHelper.CsvReader(fileReader);
                csv.Configuration.HasHeaderRecord = false;
                DataColumn dtcol = new DataColumn();
                while (csv.Read())
                {
                    DataRow dr = dt.NewRow();

                    for (int i = 0; csv.TryGetField<string>(i, out value); i++)
                    {
                        if (j == 0)
                        {
                            dt.Columns.Add(value);
                        }
                        else
                        {
                            dr[i] = value;
                        }

                        result.Add(value);
                    }
                    if (j > 0)
                    {
                        dt.Rows.Add(dr);
                    }
                    j++;
                }
            }

        }

        public void SqlBulk(string _path, string Type, uploadfile Product)
        {
            var con = new SqlConnection(new OMSEntities().Database.Connection.ConnectionString.ToString());
            try
            {
                if (con.State == System.Data.ConnectionState.Closed)
                {
                    con.Open();
                }
                int Invoiceid = fileuploadrepo.GetInvoiceId(Type, _path, Convert.ToInt32(Product.Month), Convert.ToInt32(Product.Year));
                DataTable dt = GetDataTableFromCsv(_path, true);
                dt.Columns.Add(new DataColumn("InvoiceId", typeof(int), Invoiceid.ToString()));
                dt.AcceptChanges();
                if (Type.ToLower() == "Azure".ToLower())
                {
                    ImportAzure(dt, con);
                }
                if (Type.ToLower() == "Office".ToLower())
                {
                    ImportOffice(dt, con);
                }
                if (Type.ToLower() == "OnlineService".ToLower())
                {
                    ImportOnlineServices(dt, con);
                }
                con.Close();
            }
            catch (Exception ex)
            {
                if (con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void ImportAzure(DataTable dt, SqlConnection con)
        {
            try
            {
                using (SqlBulkCopy bcp = new SqlBulkCopy(con))
                {

                    bcp.BatchSize = 100000;
                    bcp.DestinationTableName = "vt_InvoiceAzure";
                    bcp.ColumnMappings.Add("PartnerId", "PartnerId");
                    bcp.ColumnMappings.Add("PartnerName", "PartnerName");
                    bcp.ColumnMappings.Add("PartnerBillableAccountId", "PartnerBillableAccountId");
                    bcp.ColumnMappings.Add("CustomerCompanyName", "CustomerCompanyName");
                    bcp.ColumnMappings.Add("MpnId", "MpnId");
                    bcp.ColumnMappings.Add("Tier2MpnId", "Tier2MpnId");
                    bcp.ColumnMappings.Add("InvoiceNumber", "InvoiceNumber");
                    bcp.ColumnMappings.Add("ChargeStartDate", "ChargeStartDate");
                    bcp.ColumnMappings.Add("ChargeEndDate", "ChargeEndDate");
                    bcp.ColumnMappings.Add("SubscriptionId", "SubscriptionId");
                    bcp.ColumnMappings.Add("SubscriptionName", "SubscriptionName");
                    bcp.ColumnMappings.Add("SubscriptionDescription", "SubscriptionDescription");
                    bcp.ColumnMappings.Add("OrderId", "OrderId");
                    bcp.ColumnMappings.Add("ServiceName", "ServiceName");
                    bcp.ColumnMappings.Add("ServiceType", "ServiceType");
                    bcp.ColumnMappings.Add("ResourceGuid", "ResourceGuid");
                    bcp.ColumnMappings.Add("ResourceName", "ResourceName");
                    bcp.ColumnMappings.Add("Region", "Region");
                    bcp.ColumnMappings.Add("Sku", "Sku");
                    bcp.ColumnMappings.Add("DetailLineItemId", "DetailLineItemId");
                    bcp.ColumnMappings.Add("ConsumedQuantity", "ConsumedQuantity");
                    bcp.ColumnMappings.Add("IncludedQuantity", "IncludedQuantity");
                    bcp.ColumnMappings.Add("OverageQuantity", "OverageQuantity");
                    bcp.ColumnMappings.Add("ListPrice", "ListPrice");
                    bcp.ColumnMappings.Add("PretaxCharges", "PretaxCharges");
                    bcp.ColumnMappings.Add("TaxAmount", "TaxAmount");
                    bcp.ColumnMappings.Add("PostTaxTotal", "PostTaxTotal");
                    bcp.ColumnMappings.Add("Currency", "Currency");
                    bcp.ColumnMappings.Add("PretaxEffectiveRate", "PretaxEffectiveRate");
                    bcp.ColumnMappings.Add("PostTaxEffectiveRate", "PostTaxEffectiveRate");
                    bcp.ColumnMappings.Add("ChargeType", "ChargeType");
                    bcp.ColumnMappings.Add("CustomerId", "CustomerId");
                    bcp.ColumnMappings.Add("DomainName", "DomainName");
                    bcp.ColumnMappings.Add("BillingCycleType", "BillingCycleType");
                    bcp.ColumnMappings.Add("Unit", "Unit");
                    //InvoiceId
                    bcp.ColumnMappings.Add("InvoiceId", "InvoiceId");
                    bcp.BulkCopyTimeout = 0;
                    bcp.WriteToServer(dt);
                    bcp.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ImportOffice(DataTable dt, SqlConnection con)
        {
            try
            {
                using (SqlBulkCopy bcp = new SqlBulkCopy(con))
                {
                    bcp.BatchSize = 100000;
                    bcp.DestinationTableName = "vt_InvoiceOffice";
                    bcp.ColumnMappings.Add("PartnerId", "PartnerId");
                    bcp.ColumnMappings.Add("CustomerId", "CustomerId");
                    bcp.ColumnMappings.Add("CustomerName", "CustomerName");
                    bcp.ColumnMappings.Add("MpnId", "MpnId");
                    bcp.ColumnMappings.Add("Tier2MpnId", "Tier2MpnId");
                    bcp.ColumnMappings.Add("OrderId", "OrderId");
                    bcp.ColumnMappings.Add("SubscriptionId", "SubscriptionId");
                    bcp.ColumnMappings.Add("SyndicationPartnerSubscriptionNumber", "SyndicationPartnerSubscriptionNumber");
                    bcp.ColumnMappings.Add("OfferId", "OfferId");
                    bcp.ColumnMappings.Add("DurableOfferId", "DurableOfferId");
                    bcp.ColumnMappings.Add("OfferName", "OfferName");
                    bcp.ColumnMappings.Add("SubscriptionStartDate", "SubscriptionStartDate");
                    bcp.ColumnMappings.Add("SubscriptionEndDate", "SubscriptionEndDate");
                    bcp.ColumnMappings.Add("ChargeStartDate", "ChargeStartDate");
                    bcp.ColumnMappings.Add("ChargeEndDate", "ChargeEndDate");
                    bcp.ColumnMappings.Add("ChargeType", "ChargeType");
                    bcp.ColumnMappings.Add("UnitPrice", "UnitPrice");
                    bcp.ColumnMappings.Add("Quantity", "Quantity");
                    bcp.ColumnMappings.Add("Amount", "Amount");
                    bcp.ColumnMappings.Add("TotalOtherDiscount", "TotalOtherDiscount");
                    bcp.ColumnMappings.Add("Subtotal", "Subtotal");
                    bcp.ColumnMappings.Add("Tax", "Tax");
                    bcp.ColumnMappings.Add("TotalForCustomer", "TotalForCustomer");
                    bcp.ColumnMappings.Add("Currency", "Currency");
                    bcp.ColumnMappings.Add("DomainName", "DomainName");
                    bcp.ColumnMappings.Add("SubscriptionName", "SubscriptionName");
                    bcp.ColumnMappings.Add("SubscriptionDescription", "SubscriptionDescription");
                    bcp.ColumnMappings.Add("BillingCycleType", "BillingCycleType");
                    bcp.ColumnMappings.Add("InvoiceId", "InvoiceId");

                    bcp.BulkCopyTimeout = 0;
                    bcp.WriteToServer(dt);
                    bcp.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ImportOnlineServices(DataTable dt, SqlConnection con)
        {
            try
            {
                using (SqlBulkCopy bcp = new SqlBulkCopy(con))
                {
                    bcp.BatchSize = 100000;
                    bcp.DestinationTableName = "vt_Invoice";

                    bcp.ColumnMappings.Add("PartnerId", "PartnerId");
                    bcp.ColumnMappings.Add("CustomerId", "CustomerId");
                    bcp.ColumnMappings.Add("CustomerName", "CustomerName");
                    bcp.ColumnMappings.Add("CustomerDomainName", "CustomerDomainName");
                    bcp.ColumnMappings.Add("CustomerCountry", "CustomerCountry");
                    bcp.ColumnMappings.Add("InvoiceNumber", "InvoiceNumber");
                    bcp.ColumnMappings.Add("MpnId", "MpnId");
                    bcp.ColumnMappings.Add("ResellerMpnId", "ResellerMpnId");


                    bcp.ColumnMappings.Add("OrderID", "OrderID");
                    bcp.ColumnMappings.Add("OrderDate", "OrderDate");
                    bcp.ColumnMappings.Add("ProductId", "ProductId");
                    bcp.ColumnMappings.Add("SkuId", "SkuId");
                    bcp.ColumnMappings.Add("AvailabilityId", "AvailabilityId");
                    bcp.ColumnMappings.Add("SkuName", "SkuName");
                    bcp.ColumnMappings.Add("ProductName", "ProductName");
                    bcp.ColumnMappings.Add("ChargeType", "ChargeType");


                    bcp.ColumnMappings.Add("UnitPrice", "UnitPrice");
                    bcp.ColumnMappings.Add("Quantity", "Quantity");
                    bcp.ColumnMappings.Add("SubTotal", "SubTotal");
                    bcp.ColumnMappings.Add("TaxTotal", "TaxTotal");
                    bcp.ColumnMappings.Add("Total", "Total");
                    bcp.ColumnMappings.Add("Currency", "Currency");
                    bcp.ColumnMappings.Add("DiscountDetails", "DiscountDetails");
                    bcp.ColumnMappings.Add("PublisherName", "PublisherName");


                    bcp.ColumnMappings.Add("PublisherId", "PublisherId");
                    bcp.ColumnMappings.Add("SubscriptionDescription", "SubscriptionDescription");
                    bcp.ColumnMappings.Add("SubscriptionId", "SubscriptionId");
                    bcp.ColumnMappings.Add("ChargeStartDate", "ChargeStartDate");
                    bcp.ColumnMappings.Add("ChargeEndDate", "ChargeEndDate");
                    bcp.ColumnMappings.Add("TermAndBillingcycle", "TermAndBillingcycle");
                    bcp.ColumnMappings.Add("EffectiveUnitPrice", "EffectiveUnitPrice");
                    bcp.ColumnMappings.Add("UnitType", "UnitType");
                    bcp.ColumnMappings.Add("AlternateId", "AlternateId");
                    bcp.ColumnMappings.Add("InvoiceId", "InvoiceId");

                    bcp.BulkCopyTimeout = 0;
                    bcp.WriteToServer(dt);
                    bcp.Close();
                }
            }
            catch (Exception ex)
            { throw ex; }
        }
        public DataTable GetDataTableFromCsv(string path, bool isFirstRowHeader)
        {
            string header = isFirstRowHeader ? "Yes" : "No";

            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);

            string sql = @"SELECT * FROM [" + fileName + "]";

            using (OleDbConnection connection = new OleDbConnection(
                      @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathOnly +
                      ";Extended Properties=\"Text;HDR=" + header + "\""))
            using (OleDbCommand command = new OleDbCommand(sql, connection))
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
                DataTable dataTable = new DataTable();
                dataTable.Locale = CultureInfo.CurrentCulture;
                adapter.Fill(dataTable);
                return dataTable;
            }
        }
    }
}