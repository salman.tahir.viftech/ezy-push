﻿using Microsoft.Store.PartnerCenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DBEntities
{
    public class OMSSession
    {
        public int UserID { get; set; }
        public int CustomerID { get; set; }
        public Customer _Customer { get; set; }
        public vt_User user { get; set; }
        public DateTime LoginTimeStamp { get; set; }
        public IAggregatePartner User { get; set; }
        public List<vt_RolePermission> ControllerList { get; set; }

        public int Userby { get; set; }
    }
    public class Customer
    {
        public string CompanyName { get; set; }
        public string CustomerMCID { get; set; }
        public string CustomerName { get; set; }
    }
}
