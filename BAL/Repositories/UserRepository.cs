﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repositories
{
    public class UserRepository : BaseRepository
    {

        public UserRepository()
            : base()
        { }

        public UserRepository(OMSEntities ContextDB)
             : base()
        { DBContext = ContextDB; }

        public IList<DAL.DBEntities.vt_User> Getuser()
        {
            return DBContext.Database.SqlQuery<vt_User>("Select * from vt_User where IsActive=1").ToList();
            //return DBContext.Database.SqlQuery<vt_Products>("Select * from vt_Products where IsActive=1 and ProductType=@ProductType", new SqlParameter("@ProductType", Category)).ToList();
        }

        public DAL.DBEntities.vt_User GetuserbyId(int id)
        {
            return DBContext.Database.SqlQuery<vt_User>("Select * from vt_User where ID=@Userid", new SqlParameter("@Userid", id)).FirstOrDefault();
        }
        public List<DAL.DBEntities.vt_Region> GetAllRegion()
        {
            return DBContext.Database.SqlQuery<vt_Region>("select * from [vtLocalDBUser].[vt_Region]").ToList();
        }
        public List<DAL.DBEntities.vt_Role> GetAllRole()
        {
            return DBContext.Database.SqlQuery<vt_Role>("select * from [vtLocalDBUser].[vt_Role]").ToList();
        }
        public bool updateUserbyId(vt_User user)
        {
            try
            {
                //vt_User _user= DBContext.Database.SqlQuery<vt_User>("Select * from vt_User where ID=@Userid", new SqlParameter("@Userid", user.ID)).FirstOrDefault();
                DBContext.vt_User.Attach(user);
                DBContext.UpdateOnly<vt_User>(user, x => x.Email, x => x.UserName, x => x.Address, x => x.Password, x => x.MCID, x => x.PartnerAccount, x => x.FName, x => x.LName, x => x.Address, x => x.ContactNumber, x => x.RegionID, x => x.RoleID);
                DBContext.SaveChanges();
                //_user.Email = user.Email;
                //_user.UserName = user.UserName;
                //_user.Address = user.Address;
                //_user.Password = user.Password;
                //_user.MCID = user.MCID;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }
        }

        public bool InsertUser(vt_User user)
        {
            try
            {
                user.IsActive = true;
                user.Created = DateTime.Now;
                DBContext.Entry(user).State = System.Data.Entity.EntityState.Added;
                DBContext.SaveChanges();
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
                return false;
            }
        }

        public bool DeleteUserbyId(int id)
        {
            bool Status = false;
            vt_User _user = DBContext.Database.SqlQuery<vt_User>("Select * from vt_User where ID=@Userid", new SqlParameter("@Userid", id)).FirstOrDefault();
            if(_user!=null)
            {
                _user.IsActive = false;
                DBContext.vt_User.Attach(_user);
                DBContext.UpdateOnly<vt_User>(_user, x => x.IsActive);
                DBContext.SaveChanges();
                Status= true;
            }
            return Status;
            
        }
    }
}
