﻿using BAL.Repositories;
using DAL.DBEntities;
using Microsoft.Store.PartnerCenter;
using Microsoft.Store.PartnerCenter.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;
using System.Text;
using System.IO;

namespace EZY_Project.Controllers
{
    public class CustomerController : BaseController
    {
        CustomerRepository CustomerRepo;
        ProductRepository ProductRepo;
        OrderRepository OrderRepo;
        public CustomerController()
        {
            CustomerRepo = new CustomerRepository(new DAL.DBEntities.OMSEntities());
            ProductRepo = new ProductRepository(new DAL.DBEntities.OMSEntities());
            OrderRepo = new OrderRepository(new DAL.DBEntities.OMSEntities());
        }
        // GET: Customer


        [HttpPost]
        public ActionResult GetCustomersviaDatatable()
        {
            
            var allcustomer = CustomerRepo.GetCustomers(CurrentUser.Userby);
            var abc = JsonConvert.SerializeObject(allcustomer, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            var result = Json(new { data = abc }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        public ActionResult Index()
        {
            ViewBag.CountryList = CustomerRepo.getCountry();
            //SeekBasedResourceCollection<Microsoft.Store.PartnerCenter.Models.Customers.Customer> customer = CustomerRepo.GetCustomersByPartnerCenter();
            //IList<vt_Customers> customers = new IList<vt_Customers>();//customer.Items.ToList();

            return View();
            //return View(CustomerRepo.GetCustomers());
        }
        [HttpGet]
        public ActionResult Index(int id)
        {

            CurrentUser.Userby = id == 0 ? CurrentUser.user.ID : id;
            ViewBag.CountryList = CustomerRepo.getCountry();
            //SeekBasedResourceCollection<Microsoft.Store.PartnerCenter.Models.Customers.Customer> customer = CustomerRepo.GetCustomersByPartnerCenter();
            //IList<vt_Customers> customers = new IList<vt_Customers>();//customer.Items.ToList();

            return View("Index");
            //return View(CustomerRepo.GetCustomers());
        }

        public ActionResult Customer(string Customer)
        {
            return RedirectToAction("OrderHistory", "Order");
        }
        public ActionResult CreateCustomer()
        {
            ViewBag.CountryList = CustomerRepo.getCountry();
            return View();
        }

        [HttpPost]
        public ActionResult CreateCustomer(DAL.DBEntities.vt_Customers customer)
        {
            customer.UserID = CurrentUser.user.Email;
            customer.UserId = CurrentUser.Userby;
            ViewBag.CountryList = CustomerRepo.getCountry();
            bool Status = CustomerRepo.InsertCustomer(customer, CurrentUser.user.Email, CurrentUser.user.Password);

            return Json(new { status = Status }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsDomainExist(string Domain)
        {
            DAL.Response resp = CustomerRepo.CheckDomain(Domain, CurrentUser.user.Email, CurrentUser.user.Password);

            return Json(new { status = resp.status, text = resp.DomainPrefix }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Subscriptions()
        {
            Customer _customer = new Customer();
            OMSSession sess = (OMSSession)System.Web.HttpContext.Current.Session["OMSSession"];
            sess.CustomerID = Convert.ToInt32(Request.RawUrl.Split('/')[3]);
            _customer = OrderRepo.GetCustomerMicrosoftID(Convert.ToInt32(Request.RawUrl.Split('/')[3]));
            sess._Customer = _customer;
            Session.Add("OMSSession", sess);
            return RedirectToAction("Index", "Product", new { Category = "OnlineServices" });
            //return View("Index", ProductRepo.GetProducts("OnlineServices"));
        }
        public FileContentResult ExportCustomer()
        {
            var Customer = (dynamic)null;
            string DataLine = string.Empty;
            var allcustomer = CustomerRepo.GetCustomers(CurrentUser.Userby).Select(x => new { x.CustomerMCID, x.Company, x.CompanyAccount });
            string filename = "Customer" + DateTime.Now.ToString("dd/MM/yyyy") + ".csv";
            //string Path = Server.MapPath("~/UploadedFiles/Customer.csv");
            //StreamWriter str = new StreamWriter(Path);
            StringBuilder sb = new StringBuilder();
            //using (str)
            //{
            //string HeaderLine = "Microsoft Id,Company Name,Primary Domain Name,Relationship";
            sb.Append("Microsoft Id");
            sb.Append(",");
            sb.Append("Company Name");
            sb.Append(",");
            sb.Append("Primary Domain Name");
            sb.Append(",");
            sb.Append("Relationship");
            sb.AppendLine();
            //str = WriteCSVLine(HeaderLine, true, str);
            foreach (var item in allcustomer)
            {
                //DataLine = item.CustomerMCID + "," +item.Company + ","+item.CompanyAccount+","+"Cloud Resellar";
                sb.Append(item.CustomerMCID);
                sb.Append(",");
                sb.Append(item.Company );
                sb.Append(",");
                sb.Append(item.CompanyAccount+ ".onmicrosoft.com");
                sb.Append(",");
                sb.Append("Cloud Resellar");
                sb.AppendLine();
                //str = WriteCSVLine(DataLine, false, str);
            }

            Customer = sb.ToString();
            //str.Close();
            //}
            return File(new System.Text.UTF8Encoding().GetBytes(Customer), "text/csv", filename);
        }

        private StreamWriter WriteCSVLine(string Line, bool Isheader, StreamWriter str)
        {
            string HeaderLine = string.Empty;
            if (Isheader)
            {
                str.WriteLine(Line);

            }
            else
            {
                str.WriteLine(Line);
            }
            return str;
        }
    }
}