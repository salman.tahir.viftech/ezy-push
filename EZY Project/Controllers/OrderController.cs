﻿using BAL.Repositories;
using DAL;
using DAL.DBEntities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EZY_Project.Controllers
{
    //Test
    public class OrderController : BaseController
    {
        public OrderRepository Orderrepo;
        public OrderController()
        {
            Orderrepo = new OrderRepository(new OMSEntities());
        }
        // GET: Order
        public ActionResult Index()
        {
            Order order = new Order();
            order._Customer = CurrentUser._Customer;
            //order.CustomerName=CurrentUser.
            IList<vt_Cart> carts = JsonConvert.DeserializeObject<List<vt_Cart>>((Request.Cookies["Cart"]).Value);
            int[] arrayCartID = carts.Select(x => x.ID).ToArray();
            order.Cart = Orderrepo.GetOrder(arrayCartID);
            return View(order);
        }

        [HttpGet]
        public ActionResult OrderConfirmation()
        {
            bool Status = false;
            string message = string.Empty;
            try
            {
                IList<vt_Cart> carts = JsonConvert.DeserializeObject<List<vt_Cart>>((Request.Cookies["Cart"]).Value);
                int[] arrayCartID = carts.Select(x => x.ID).ToArray();
                Status=Orderrepo.CreateOrder(arrayCartID,CurrentUser.user.Email,CurrentUser.user.Password,CurrentUser.User);
            }
            catch(Exception ex)
            {
                message = ex.Message;
            }


            return Json(new { success = Status,message= message }, JsonRequestBehavior.AllowGet);//RedirectToAction("Index", "Product");
        }


        public ActionResult Modules()
        {            
            Customer _customer = new Customer();
            OMSSession sess = (OMSSession)System.Web.HttpContext.Current.Session["OMSSession"];
            sess.CustomerID = Convert.ToInt32(Request.RawUrl.Split('/')[3]);
            _customer = Orderrepo.GetCustomerMicrosoftID(Convert.ToInt32(Request.RawUrl.Split('/')[3]));
            sess._Customer = _customer;
            Session.Add("OMSSession", sess);
            //Request.RawUrl.Split('/')[3]
            return View("OrderHistory", _customer);
            //return View(Orderrepo.GetOrderbyCustomerID(Convert.ToInt32(Request.RawUrl.Split('/')[3])));
        }

        public ActionResult Orders()
        {
            return PartialView("_Order", Orderrepo.GetOrderbyCustomerID(CurrentUser.CustomerID));
        }

        public ActionResult Subscriptions()
        {
            return PartialView("_Subscriptions", Orderrepo.GetSubscriptionByCustomerID(CurrentUser.CustomerID));
        }
    }
}