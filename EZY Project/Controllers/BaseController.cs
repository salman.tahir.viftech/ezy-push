﻿using DAL.DBEntities;
using SDViewModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EZY_Project.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        OMSSession sess;


        public OMSSession CurrentUser
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["OMSSession"] != null)
                {
                    sess = (OMSSession)System.Web.HttpContext.Current.Session["OMSSession"];
                }
                return sess;
            }
            set
            {
                sess = value;
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string redirectController = string.Empty;
            string redirectAction = string.Empty;
            string ErrorMessage = string.Empty;
            bool IsvalidToken = true;
            bool authorizedRequest = true;
            var urlHelper = new UrlHelper(filterContext.RequestContext);
            if (System.Web.HttpContext.Current.Session["OMSSession"] != null)
            {
                OMSSession sess = (OMSSession)System.Web.HttpContext.Current.Session["OMSSession"];
                var rd = System.Web.HttpContext.Current.Request.RequestContext.RouteData;
                string currentController = rd.GetRequiredString("controller");
                string currentAction = rd.GetRequiredString("action");
                if (sess != null)
                {

                    string Page_URL = string.Empty;                      
                    Page_URL = currentController;
                   
                    var right = sess.ControllerList.Where(x => x.Menu.ToLower() == Page_URL.ToLower()).FirstOrDefault();
                   
                    if (right == null)
                    {
                        redirectController = "Error";
                        redirectAction = "Permission";
                        ErrorMessage = "UnAuthorized";
                        authorizedRequest = false;

                    }
                    else
                    {
                        redirectController = currentController;
                        redirectAction = currentAction;
                        ErrorMessage = "Authorized";
                        authorizedRequest = true;
                    }

                    if (!authorizedRequest)
                    {
                        filterContext.Result = new RedirectResult("~/" + redirectController + "/" + redirectAction);
                    }


                }
            }
            else
            {

                filterContext.Result = new RedirectResult("~/Login");

            }


            base.OnActionExecuting(filterContext);
        }

            //string redirectController = string.Empty;
            //string redirectAction = string.Empty;
            //string ErrorMessage = string.Empty;
            //bool IsvalidToken = true;
            //bool authorizedRequest = true;
            //    if (System.Web.HttpContext.Current.Session["TNASession"] != null)
            //    {
            //        TNA_Session sess = (TNA_Session)System.Web.HttpContext.Current.Session["TNASession"];
            //var rd = System.Web.HttpContext.Current.Request.RequestContext.RouteData;
            //string currentController = rd.GetRequiredString("controller");
            //string currentAction = rd.GetRequiredString("action");
            //        if (sess != null)
            //        {

            //            string Page_URL = string.Empty;
            //            if (!string.IsNullOrEmpty(vt_Common.GetPathPrefix()))
            //                Page_URL = "/" + vt_Common.GetPathPrefix() + "/" + currentController + "/" + currentAction;
            //            else
            //                Page_URL = "/" + currentController + "/" + currentAction;
            //                //string Page_URL = vt_Common.GetPathPrefix() + "/"+ currentController + "/" + currentAction; 
            //                var right = sess.PageList.Where(x => x.PageURL.ToLower() == Page_URL.ToLower()).FirstOrDefault();
            //                //var right = sess.pagelist.Where(x => x.PageURL.ToLower() == Page_URL.ToLower() && x.Controller.ToLower() == currentController.ToLower()).FirstOrDefault();
            //                if (right == null)
            //                {
            //                    redirectController = "Error";
            //                    redirectAction = "Permission";
            //                    ErrorMessage = "UnAuthorized";
            //                    authorizedRequest = false;

            //                }
            //                else
            //                {
            //                    redirectController = currentController;
            //                    redirectAction = currentAction;
            //                    ErrorMessage = "Authorized";
            //                    authorizedRequest = true;
            //                }




            //        }


            //    }
            //    else
            //    {

            //        filterContext.Result = new RedirectResult("~/Login");

            //    }


            //    base.OnActionExecuting(filterContext);
        }
}