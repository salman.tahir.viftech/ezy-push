﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BAL.Repositories
{
    public static class RenderPartial
    {
        public static string RenderToString(this PartialViewResult partialView, ControllerContext context)
        {
            string returnvalue = string.Empty;
            try
            {
                string viewName;
                if (string.IsNullOrEmpty(partialView.ViewName))
                    viewName = context.RouteData.GetRequiredString("action");
                else
                    viewName = partialView.ViewName;

                ViewDataDictionary viewData = new ViewDataDictionary(partialView.Model);

                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
                    ViewContext viewContext = new ViewContext(context, viewResult.View, viewData, new TempDataDictionary(), sw);
                    viewResult.View.Render(viewContext, sw);

                    returnvalue = sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return returnvalue;


        }

        public static string get_setting(string name, string default_value)
        {
            NameValueCollection name_values = (NameValueCollection)System.Configuration.ConfigurationManager.GetSection("appSettings");
            if (string.IsNullOrEmpty(name_values[name]))
            {
                return default_value;
            }
            else
            {
                return name_values[name];
            }
        }
    }
}
