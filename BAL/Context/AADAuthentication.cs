﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Store.PartnerCenter;
using System;
using Microsoft.Store.PartnerCenter.Samples.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Store.PartnerCenter.Extensions;
using System.Threading.Tasks;

namespace Partner_Center.Context
{
    public class AADAuthentication
    {

        /// <summary>
        /// A lazy reference to an user based partner operations.
        /// </summary>
        private IAggregatePartner userPartnerOperations = null;

        public string UserName { get; set; }

        public string Password { get; set; }
        /// <summary>
        /// Gets a configuration instance.
        /// </summary>
        public ConfigurationManager Configuration
        {
            get
            {
                return ConfigurationManager.Instance;
            }
        }

        public AADAuthentication(string UserName, string Password)
        {
            this.UserName = UserName;
            this.Password = Password;
        }
        public AADAuthentication()
        {
            PartnerService.Instance.ApiRootUrl = this.Configuration.PartnerService.PartnerServiceApiEndpoint.ToString();
            PartnerService.Instance.ApplicationName = "Partner Center .NET SDK Samples";
        }

        public IAggregatePartner UserPartnerOperation
        {
            get
            {
                if (this.userPartnerOperations == null)
                {
                    //this.ConsoleHelper.StartProgress("Authenticating user");
                    var aadAuthenticationResult = this.LoginUserToAad();

                    // Authenticate by user context with the partner service
                    IPartnerCredentials userCredentials = PartnerCredentials.Instance.GenerateByUserCredentials(
                        Configuration.UserAuthentication.ApplicationId,
                        new AuthenticationToken(
                            aadAuthenticationResult.AccessToken,
                            aadAuthenticationResult.ExpiresOn),
                        delegate
                        {
                            // token has expired, re-Login to Azure Active Directory
                            //this.ConsoleHelper.StartProgress("Token expired. Re-authenticating user");
                            var aadToken = this.LoginUserToAad();
                            //this.ConsoleHelper.StopProgress();

                            // give the partner SDK the new add token information
                            return Task.FromResult(new AuthenticationToken(aadToken.AccessToken, aadToken.ExpiresOn));
                        });

                    //this.ConsoleHelper.StopProgress();
                    //this.ConsoleHelper.Success("Authenticated!");

                    this.userPartnerOperations = PartnerService.Instance.CreatePartnerOperations(userCredentials);
                }

                return this.userPartnerOperations;
            }
        }

        /// <summary>
        /// Logs in to AAD as a user and obtains the user authentication token.
        /// </summary>
        /// <returns>The user authentication result.</returns>
        private AuthenticationResult LoginUserToAad()
        {
            var addAuthority = new UriBuilder(this.Configuration.PartnerService.AuthenticationAuthorityEndpoint)
            {
                Path = this.Configuration.PartnerService.CommonDomain
            };

            UserCredential userCredentials = new UserCredential(
                UserName,
                Password);

            AuthenticationContext authContext = new AuthenticationContext(addAuthority.Uri.AbsoluteUri);

            return authContext.AcquireToken(
                this.Configuration.UserAuthentication.ResourceUrl.OriginalString,
                this.Configuration.UserAuthentication.ApplicationId,
                userCredentials);
        }
    }
}