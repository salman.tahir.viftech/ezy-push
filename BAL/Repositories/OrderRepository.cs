﻿using DAL.DBEntities;
using EZY_Project.PCModules;
using Microsoft.Store.PartnerCenter.Models;
using Microsoft.Store.PartnerCenter.Models.Orders;
using Microsoft.Store.PartnerCenter.Models.Subscriptions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repositories
{
    public class OrderRepository : BaseRepository
    {
        public OrderRepository()
            : base()
        {

        }
        public OrderRepository(OMSEntities ContextDB)
            : base()
        {
            DBContext = ContextDB;
        }

        public IList<vt_Cart> GetOrder(int[] cartIDs)
        {
            return DBContext.vt_Cart.Where(x => cartIDs.Contains(x.ID) && x.IsActive == true).ToList();
        }

        public bool CreateOrder(int[] cartIDs, string UserName, string Password, Microsoft.Store.PartnerCenter.IAggregatePartner partner)
        {
            bool Status = false;
            using (DbContextTransaction transaction = DBContext.Database.BeginTransaction())
            {
                try
                {

                    var cart = DBContext.ExclueAll().vt_Cart.Where(x => cartIDs.Contains(x.ID) && x.IsActive == true).ToList();
                    foreach (var item in DBContext.ExclueAll().vt_Cart.Where(x => cartIDs.Contains(x.ID) && x.IsActive == true).ToList())
                    {
                        OrderComplete(item);
                        if (item.BillingCycle.Contains("One Time"))
                        {
                            CreateOrder(new OrderPlacement(UserName, Password,partner).CreateOrderSoftware(item), item, UserName, Password,partner);
                        }
                        else
                        {
                            CreateOrder(new OrderPlacement(UserName, Password, partner).CreateOrder(item), item, UserName, Password, partner);
                        }
                        //CreateOrder(new OrderPlacement(UserName,Password).CreateOrder(item), item);
                        DBContext.SaveChanges();
                    }

                    transaction.Commit();
                    Status = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Status = false;
                    throw ex;
                }
            }
            return Status;
        }


        public void CreateOrder(Order _order, vt_Cart cart, string UserName, string Password, Microsoft.Store.PartnerCenter.IAggregatePartner partner)
        {
            try
            {
                var ProductDetails = DBContext.ExclueAll().vt_ProductDetails.Where(x => x.MCID == cart.P_MCID && x.SKUID == cart.SKUID).FirstOrDefault();
                vt_Order _orderplacement = new vt_Order();
                _orderplacement.BillingCycle = _order.BillingCycle.ToString();
                _orderplacement.O_MCID = _order.Id.ToString();
                _orderplacement.S_MCID = _order.LineItems.Select(x => x.SubscriptionId).FirstOrDefault();
                _orderplacement.C_MCID = _order.ReferenceCustomerId;
                _orderplacement.ProductId = cart.ProductID;
                _orderplacement.P_MCID = cart.P_MCID;
                _orderplacement.SKU_MCID = cart.SKUID;
                _orderplacement.customerId = cart.CustomerID;
                _orderplacement.Offer_MCID = _order.LineItems.Select(x => x.OfferId).FirstOrDefault();
                _orderplacement.Title = _order.LineItems.Select(x => x.FriendlyName).FirstOrDefault();
                _orderplacement.Quantity = _order.LineItems.Select(x => x.Quantity).FirstOrDefault();
                _orderplacement.CurrencyCode = _order.CurrencyCode;
                _orderplacement.CreationDate = _order.CreationDate;
                _orderplacement.Status = _order.Status;
                if (_order.BillingCycle.ToString() == "OneTime")
                {
                    _orderplacement.Duration = cart.Duration;
                    _orderplacement.ListPrice = _orderplacement.Quantity * ProductDetails.ListPrice;
                    _orderplacement.Msrp = _orderplacement.Quantity * ProductDetails.Msrp;
                }
                else
                {
                    CreateSubscription(_order, _orderplacement,UserName,Password,partner);
                }

                DBContext.Entry(_orderplacement).State = System.Data.Entity.EntityState.Added;
                //DBContext.SaveChanges();
                //_orderplacement.ProvisioningStatus = _order.Links.ProvisioningStatus.Uri.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreateSubscription(Order _order, vt_Order _orderplacement,string UserName, string Password, Microsoft.Store.PartnerCenter.IAggregatePartner partner)
        {
            try
            {
                ResourceCollection<Microsoft.Store.PartnerCenter.Models.Subscriptions.Subscription> Sub = new PCSubscription(UserName,Password,partner).GetSubscriptionByOrderId(_order.ReferenceCustomerId, _order.Id);
                vt_Subscription _subscription = new vt_Subscription();
                _subscription.S_MCID = Sub.Items.Select(x => x.Id).FirstOrDefault().ToString();
                _subscription.M_EntitlementId = Sub.Items.Select(x => x.EntitlementId).FirstOrDefault().ToString();
                _subscription.M_OfferId = Sub.Items.Select(x => x.OfferId).FirstOrDefault().ToString();
                _subscription.OfferName = Sub.Items.Select(x => x.OfferName).FirstOrDefault().ToString();
                _subscription.FriendlyName = Sub.Items.Select(x => x.FriendlyName).FirstOrDefault().ToString();
                _subscription.Quantity = Sub.Items.Select(x => x.Quantity).FirstOrDefault();
                _subscription.UnitType = Sub.Items.Select(x => x.UnitType).FirstOrDefault().ToString();
                _subscription.CreateDate = Sub.Items.Select(x => x.CreationDate).FirstOrDefault();
                _subscription.EffectiveStartDate = Sub.Items.Select(x => x.EffectiveStartDate).FirstOrDefault();
                _subscription.CommitmentEndDate = Sub.Items.Select(x => x.CommitmentEndDate).FirstOrDefault();
                
                _subscription.Status = Sub.Items.Select(x => x.Status).FirstOrDefault() == SubscriptionStatus.Active ? true : false;
                _subscription.AutoRenewEnabled = Sub.Items.Select(x => x.AutoRenewEnabled).FirstOrDefault();
                _subscription.BillingType = Sub.Items.Select(x => x.BillingType).FirstOrDefault().ToString();
                _subscription.BillingCycle = Sub.Items.Select(x => x.BillingCycle).FirstOrDefault().ToString();
                _subscription.HasPurchasableAddons = Sub.Items.Select(x => x.HasPurchasableAddons).FirstOrDefault();
                _subscription.IsTrial = Sub.Items.Select(x => x.IsTrial).FirstOrDefault();
                _subscription.ContractType = Sub.Items.Select(x => x.ContractType).FirstOrDefault().ToString();
                _subscription.M_OrderId = Sub.Items.Select(x => x.OrderId).FirstOrDefault().ToString();
                _subscription.CustomerId = _orderplacement.customerId;
                _subscription.OrderId = _orderplacement.Id;
                _subscription.ProductID = _orderplacement.ProductId;
                //_subscription.UnitType = Sub.Items.Select(x => x.UnitType).FirstOrDefault().ToString();
                DBContext.Entry(_subscription).State = System.Data.Entity.EntityState.Added;
                //DBContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void OrderComplete(vt_Cart cart)
        {
            //var result = DBContext.vt_Cart.Where(x => x.ID== cart.ID).FirstOrDefault();
            //if (result != null)
            //{
            cart.IsActive = false;
            DBContext.vt_Cart.Attach(cart);
            DBContext.Entry(cart).State = EntityState.Modified;
            //DBContext.SaveChanges();
            //}
        }
        public Customer GetCustomerMicrosoftID(int CustomerID)
        {
            Customer _customer = new Customer();
            var _CustomerObject = DBContext.ExclueAll().vt_Customers.Where(x => x.ID == CustomerID).Select(x => new { x.Company ,x.CustomerMCID, x.PrimaryFirstName, x.PrimaryLastName }).FirstOrDefault();
            _customer.CompanyName = _CustomerObject.Company;
            _customer.CustomerMCID = _CustomerObject.CustomerMCID;
            _customer.CustomerName = _CustomerObject.PrimaryFirstName + " " + _CustomerObject.PrimaryLastName;
            return _customer;
        }
        public IList<vt_Order> GetOrderbyCustomerID(int CustomerID)
        {
            return DBContext.ExclueAll().vt_Order.Include("vt_Products.vt_ProductDetails").Where(x => x.customerId == CustomerID).ToList();
        }

        public IList<vt_Subscription> GetSubscriptionByCustomerID(int CustomerID)
        {
            return DBContext.ExclueAll().vt_Subscription.Where(x => x.CustomerId == CustomerID).ToList();
        }
    }
}
