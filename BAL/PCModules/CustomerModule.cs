﻿using DAL.DBEntities;
using Microsoft.Store.PartnerCenter;
using Microsoft.Store.PartnerCenter.Models;
using Microsoft.Store.PartnerCenter.Models.Customers;
using Partner_Center.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.PCModules
{
    public class CustomerModule : AADAuthentication
    {
        public static IAggregatePartner partnerOperation = null;

        public CustomerModule(string UserName,string Password)
            :base(UserName,Password)
        {
            partnerOperation = this.UserPartnerOperation;
        }
        public Microsoft.Store.PartnerCenter.Models.Customers.Customer CreateCustomers(vt_Customers _customer)
        {
            var customerToCreate = new Microsoft.Store.PartnerCenter.Models.Customers.Customer()
            {
                CompanyProfile = new CustomerCompanyProfile()
                {
                    Domain = _customer.CompanyAccount + ".onmicrosoft.com"//string.Format(CultureInfo.InvariantCulture, "SampleApplication{0}.{1}", new Random().Next(), this.Context.Configuration.Scenario.CustomerDomainSuffix)
                },
                BillingProfile = new CustomerBillingProfile()
                {
                    FirstName = string.Concat(_customer.PrimaryFirstName, "A"),
                    LastName = string.Concat(_customer.PrimaryLastName, "A"),
                    Culture = "ur-PK",
                    Email = _customer.PrimaryEmail,//"gena@relecloud2.com",
                    Language = "en",
                    CompanyName = _customer.Company + "A",//"Relecloud" + new Random().Next(),
                    DefaultAddress = new Address()
                    {
                        FirstName = _customer.PrimaryFirstName,//"Gena",
                        LastName = _customer.PrimaryLastName,//"Soto",
                        AddressLine1 = _customer.Address1,//"4567 Main Street",
                        City = _customer.City,//"Redmond",
                        State = _customer.State_Province,//"WA",
                        Country = _customer.CountryID,//"US",
                        PhoneNumber = _customer.PrimaryPhone,//"4255550101",
                        PostalCode = _customer.Zip//"98052"
                    }
                }
            };

            Microsoft.Store.PartnerCenter.Models.Customers.Customer newCustomer = partnerOperation.Customers.Create(customerToCreate);
            return newCustomer;
        }

        public SeekBasedResourceCollection<Microsoft.Store.PartnerCenter.Models.Customers.Customer> GetCustomers()
        {
            return partnerOperation.Customers.Get();
        }
    }
}
