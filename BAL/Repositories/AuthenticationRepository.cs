﻿using DAL.DBEntities;
using Microsoft.Store.PartnerCenter;
using Partner_Center.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repositories
{
    public class AuthenticationRepository
        : BaseRepository
    {
        public AuthenticationRepository()
            : base()
        {

        }

        public AuthenticationRepository(OMSEntities ContextDB)
            : base(ContextDB)
        {
            DBContext = ContextDB;
        }

        public OMSSession AuthenticateUser(vt_User user)
        {
            OMSSession session = new OMSSession();
            //IAggregatePartner authenticUser;
            var authenticUser = DBContext.ExclueAll().Database.SqlQuery<vt_User>("Select * from vt_User where Email=@Email and Password=@Password", new SqlParameter("@Email", user.Email), new SqlParameter("@Password", user.Password)).FirstOrDefault();
            //var authenticUser = DBContext.ExclueAll().vt_User.Where(x => x.Email == user.Email && x.Password == user.Password).SingleOrDefault();
            if (authenticUser != null)
            {
                IAggregatePartner __user = new AADAuthentication(authenticUser.Email, authenticUser.Password).UserPartnerOperation;
                if (__user != null)
                {
                    session.user = authenticUser;
                    session.User = __user;
                    session.ControllerList= DBContext.ExclueAll().Database.SqlQuery<vt_RolePermission>("Select * from vt_RolePermission where RoleID=@RoleID", new SqlParameter("@RoleID", authenticUser.RoleID)).ToList();
                    return session;
                }
                //authenticUser =new  AADAuthentication().
            }
            return null;
            //return authenticUser;
        }
    }
}
