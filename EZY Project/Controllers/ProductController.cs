﻿using BAL.Repositories;
using DAL;
using DAL.DBEntities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EZY_Project.Controllers
{
    //Test
    public class ProductController : BaseController
    {
        ProductRepository ProductRepo;
        public ProductController()
        {
            ProductRepo = new ProductRepository(new OMSEntities());
        }
        // GET: Product
        [Route("Product/Index")]
        public ActionResult Index(string Category)
        {
            //DAL.Product Product = new DAL.Product();
            //Product.Products = ProductRepo.GetProducts(Category);
            //Product.ProductDetail=ProductRepo.GetProductDetails();
            return View(ProductRepo.GetProducts(Category));
        }

        [HttpPost]
        public ActionResult ProductDetails(string ProductID)
        {
            return PartialView("_ProductDetails", ProductRepo.GetProductDetails(ProductID));
        }

        [HttpPost]
        public ActionResult CreateCart(vt_Cart cartmodel)
        {
            Cart _cart = new Cart();
            cartmodel.Title = cartmodel.Title.Contains(" – ") ? cartmodel.Title.Replace(" – ", " ") : cartmodel.Title;
            List<vt_Cart> Carts = new List<vt_Cart>();
            bool Status = false;
            try
            {
                //cart = ProductRepo.CreateCart(cartmodel);
                cartmodel.C_MCID = CurrentUser._Customer.CustomerMCID;
                cartmodel.CustomerID = CurrentUser.CustomerID;
                if (Request.Cookies["Cart"] != null)
                {

                    Carts = JsonConvert.DeserializeObject<List<vt_Cart>>((Request.Cookies["Cart"]).Value);//new JavaScriptSerializer().Deserialize<List<vt_Cart>>((Request.Cookies["Cart"]).Value);
                    var previouscart = Carts.Where(x => x.P_MCID == cartmodel.P_MCID).FirstOrDefault();
                    if (previouscart != null)
                    {
                        previouscart.Quantity += cartmodel.Quantity;
                        //cartmodel.Quantity += previouscart.Quantity;
                        //Carts.Remove(previouscart);
                        //Carts.Add(cartmodel);
                    }
                    else
                    {
                        Carts.Add(cartmodel);
                    }
                    
                    //string ConvertTojson = JsonConvert.SerializeObject(_cart.Carts); //new JavaScriptSerializer().Serialize(_cart);
                    //_cart.cartModel = new JavaScriptSerializer().Deserialize<vt_Cart>(Request.Cookies["Cart"].ToString());
                    //_cart.Carts.Add()


                    //HttpCookie Order = new HttpCookie("Cart");
                    //Order.Value = ConvertTojson;
                    ////// Add the cookie.  
                    //Response.Cookies.Add(Order);
                }
                else
                {
                    Carts.Add(cartmodel);
                    //string ConvertTojson = JsonConvert.SerializeObject(_cart.Carts);//new JavaScriptSerializer().Serialize(_cart);
                    //HttpCookie Order = new HttpCookie("Cart");
                    //Order.Value = ConvertTojson;
                    ////// Add the cookie.  
                    //Response.Cookies.Add(Order);
                }
                SaveCookie(Carts);
                //string ConvertTojson = JsonConvert.SerializeObject(_cart.Carts);
                //HttpCookie Order = new HttpCookie("Cart");
                //Order.Value = ConvertTojson;
                ////// Add the cookie.  
                //Response.Cookies.Add(Order);

                Status = true;
            }
            catch (Exception ex)
            {
                Request.Cookies.Remove("Cart");
                Status = false;
            }

            return Json(new { status = Status, Cart = cartmodel }, JsonRequestBehavior.AllowGet);
            //return PartialView("_Cart", cart);
        }
        //[HttpGet]
        //public ActionResult CartReview(string id)
        //{
        //    int[] array = id.Split(',').Select(str => int.Parse(str)).ToArray();
        //    //int[] int_arr = Array.ConvertAll(id, Int32.Parse);
        //    //string Carts = string.Join(",", CartId);
        //    return View(ProductRepo.getList(array));
        //}

        public ActionResult CartReview()
        {
            //List<vt_Cart> cartList = new List<vt_Cart>();
            var Cart = JsonConvert.DeserializeObject<List<vt_Cart>>((Request.Cookies["Cart"]).Value);
            //foreach (var item in Cart)
            //{
            //    //item.CustomerID = CurrentUser.CustomerID;
            //    //item.C_MCID = CurrentUser._Customer.CustomerMCID;
            //    cartList.Add(ProductRepo.CreateCartByCookies(item,CurrentUser.user.Email, CurrentUser.user.Password));
            //}

            if (Cart.Where(x => x.ID == 0).Count() > 0)
            {
                List<vt_Cart> cartList = ProductRepo.CreateCartByCookies(Cart.Where(x => x.ID == 0).ToList(), CurrentUser.user.Email, CurrentUser.user.Password, CurrentUser.User);
                //cartList.AddRange(Cart.Where(x => x.ID > 0).ToList());

                //Cart= ProductRepo.CreateCartByCookies(Cart.Where(x => x.ID == 0).ToList(), CurrentUser.user.Email, CurrentUser.user.Password, CurrentUser.User);

                SaveCookie(Cart);
            }

            //string ConvertTojson = JsonConvert.SerializeObject(cartList);
            //HttpCookie Order = new HttpCookie("Cart");
            //Order.Value = ConvertTojson;
            ////// Add the cookie.  
            //Response.Cookies.Add(Order);
            int[] array = Cart.Select(str => str.ID).ToArray();
            return View(ProductRepo.getList(array));
            //return View(cartList);
        }


        public ActionResult Confirmation(string cartList)
        {
            var Cartlist = JsonConvert.DeserializeObject<List<vt_Cart>>((Request.Cookies["Cart"]).Value);
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(cartList);
            foreach (DataRow row in dt.Rows)
            {
                vt_Cart cart = Cartlist.Where(x => x.ID == Convert.ToInt32(row["ID"])).FirstOrDefault();

                //vt_Cart cart = new vt_Cart();
                //cart.ID = Convert.ToInt32(row["ID"]);
                cart.Quantity = Convert.ToInt32(row["Quantity"]);
                cart.BillingCycle = row["BillingCycle"].ToString();
                Cartlist.Remove(cart);
                Cartlist.Add(cart);
                ProductRepo.Updatecart(cart);
                //UpdateCart(cart);
            }
            SaveCookie(Cartlist);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);//RedirectToAction("Index", "Order");
        }

        public void UpdateCart(vt_Cart _cart)
        {
            var Cartlist = JsonConvert.DeserializeObject<List<vt_Cart>>((Request.Cookies["Cart"]).Value);
            vt_Cart cart = Cartlist.Where(x => x.ID == _cart.ID).FirstOrDefault();
            Cartlist.Remove(cart);
            cart.Quantity = _cart.Quantity;
            cart.BillingCycle = _cart.BillingCycle;
            Cartlist.Add(cart);
            SaveCookie(Cartlist);
        }

        public ActionResult RemoveProduct(string P_MCID)
        {
            Cart _cart = new Cart();
            _cart.Carts = new List<vt_Cart>();
            try
            {
                if (Request.Cookies["Cart"] != null)
                {
                    _cart.Carts = JsonConvert.DeserializeObject<List<vt_Cart>>((Request.Cookies["Cart"]).Value);
                    _cart.cartModel = _cart.Carts.Where(x => x.P_MCID == P_MCID).FirstOrDefault();
                    if (!Request.RawUrl.Contains("Order/Modules"))
                    {
                        ProductRepo.RemoveFromCart(P_MCID, _cart.cartModel.C_MCID);
                    }
                    _cart.Carts.Remove(_cart.cartModel);


                    string ConvertTojson = JsonConvert.SerializeObject(_cart.Carts);

                    HttpCookie Order = Request.Cookies["Cart"];
                    Order.Value = ConvertTojson;
                    //// Add the cookie.  
                    Response.SetCookie(Order);
                }
            }
            catch (Exception ex)
            {

            }
            return Json(new { status = "Removed" }, JsonRequestBehavior.AllowGet);
        }


        public void SaveCookie(List<vt_Cart> cartList)
        {
            string ConvertTojson = JsonConvert.SerializeObject(cartList);

            HttpCookie Order = Request.Cookies["Cart"];

            if (Order != null)
            {
                Order.Value = ConvertTojson;
                Order.Expires = DateTime.UtcNow.AddHours(1);
                //Response.SetCookie(Order);
                Response.Cookies.Set(Order);
            }
            else
            {
                Order = new HttpCookie("Cart");
                Order.Value = ConvertTojson;
                //// Add the cookie.  
                Order.Expires = DateTime.UtcNow.AddHours(1);
                Response.Cookies.Add(Order);
            }

        }

        public ActionResult CancelCart()
        {
            try
            {
                if (Request.Cookies["Cart"] != null)
                {
                    ProductRepo.CancelCart(CurrentUser._Customer.CustomerMCID, CurrentUser.LoginTimeStamp);
                    //    _cart.Carts = JsonConvert.DeserializeObject<List<vt_Cart>>((Request.Cookies["Cart"]).Value);
                    //    _cart.cartModel = _cart.Carts.Where(x => x.P_MCID == P_MCID).FirstOrDefault();
                    //    if (!Request.RawUrl.Contains("Order/Modules"))
                    //    {
                    //        ProductRepo.RemoveFromCart(P_MCID, _cart.cartModel.C_MCID);
                    //    }
                    //    _cart.Carts.Remove(_cart.cartModel);


                    //    string ConvertTojson = JsonConvert.SerializeObject(_cart.Carts);

                    //    HttpCookie Order = Request.Cookies["Cart"];
                    //    Order.Value = ConvertTojson;
                    //    //// Add the cookie.  
                    //    Response.SetCookie(Order);
                }
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("Index", "Customer");
        }
    }
}