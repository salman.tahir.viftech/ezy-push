﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DBEntities
{
    public class InvoiceStructure
    {
        public vt_Customers _Customer { get; set; }
        public IList<vt_Invoice> _Invoice { get; set; }

        public IList<vt_InvoiceAzure> _Invoiceazure { get; set; }

        public IList<vt_InvoiceOffice> _Invoiceoffice { get; set; }
    }

    public class OneTimeBilling
    {
        public vt_Customers _Customer { get; set; }
        public IList<vt_Invoice> _Invoice { get; set; }

        public IList<vt_InvoiceAzure> _Invoiceazure { get; set; }

        public IList<vt_InvoiceOffice> _Invoiceoffice { get; set; }
    }
}
