﻿using DAL.DBEntities;
using Microsoft.Store.PartnerCenter;
using Microsoft.Store.PartnerCenter.Models.Offers;
using Microsoft.Store.PartnerCenter.Models.Orders;
using Partner_Center.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EZY_Project.PCModules
{
    public class OrderPlacement : AADAuthentication
    {
        public static IAggregatePartner partnerOperation = null;
        public OrderPlacement(string UserName, string Password, IAggregatePartner partner)
            : base(UserName, Password)
        {
            if (partner == null)
            { partnerOperation = this.UserPartnerOperation; }
            else
            {
                partnerOperation = partner;
            }
            //partnerOperation = this.UserPartnerOperation;
        }

        public Order CreateOrder(vt_Cart cart)
        {
            try
            {
                var order = new Order()
                {
                    BillingCycle = (BillingCycleType)Enum.Parse(typeof(BillingCycleType), cart.BillingCycle),
                    ReferenceCustomerId = cart.C_MCID,
                    LineItems = new List<OrderLineItem>()
                {
                    new OrderLineItem()
                    {
                        OfferId = cart.SKUID,
                        FriendlyName = cart.Title,
                        Quantity = (int)cart.Quantity
                    }
                }
                };

                return partnerOperation.Customers.ById(cart.C_MCID).Orders.Create(order);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void GetCustomerOrders(string CustomerID)
        {
            var customerOrders = partnerOperation.Customers.ById(CustomerID).Orders.Get();
        }
        public void GetOrderDetail(string CustomerID, string OrderID)
        {
            var customerOrderDetails = partnerOperation.Customers.ById(CustomerID).Orders.ById(OrderID).Get();
        }
        public void UpdateCustomerOrder(string CustomerID, string OrderID)
        {
            var customerOrder = partnerOperation.Customers.ById(CustomerID).Orders.ById(OrderID).Get();

            // increase the quantity of first line item
            customerOrder.LineItems.ToArray()[0].Quantity++;


            var updatedOrder = partnerOperation.Customers.ById(CustomerID).Orders.ById(customerOrder.Id).Patch(customerOrder);
        }
        public Order CreateOrderSoftware(vt_Cart cart)
        {
            string message = string.Empty;
            try
            {
                var sku = partnerOperation.Products.ByCountry("PK").ById(cart.P_MCID).Skus.ById(cart.SKUID).Get();
                var availabilities = partnerOperation.Products.ByCountry("PK").ById(cart.P_MCID).Skus.ById(cart.SKUID).Availabilities.Get();

                if ((sku.DynamicAttributes == null) || string.IsNullOrEmpty(Convert.ToString(sku.DynamicAttributes["duration"])))
                {
                    message = "Invalid Azure catalog item ID.";
                }
                else
                {
                    if (!availabilities.Items.Any())
                    {
                        message = "No availabilities found.";
                    }
                    else
                    {
                        var order = new Order()
                        {
                            ReferenceCustomerId = cart.C_MCID,
                            BillingCycle = BillingCycleType.OneTime,
                            LineItems = new List<OrderLineItem>()
                        {
                            new OrderLineItem()
                            {
                                OfferId = availabilities.Items.First().CatalogItemId,
                                FriendlyName = cart.Title.Length>25?cart.Title.Substring(0,24):cart.Title,
                                Quantity = (int)cart.Quantity,
                                LineItemNumber = 0
                                //,
                                //ProvisioningContext = new Dictionary<string, string>()
                                //{
                                //    { "subscriptionId", subscriptionId },
                                //    { "scope", "shared" },
                                //    { "duration", Convert.ToString(sku.DynamicAttributes["duration"]) }
                                //}
                            }
                        }
                        };
                        return partnerOperation.Customers.ById(cart.C_MCID).Orders.Create(order);
                    }

                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    //throw message;
                }
                else
                {
                    throw ex;
                }
            }
            return null;
        }
    }
}