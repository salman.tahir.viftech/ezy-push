﻿using BAL.Repositories;
using DAL.DBEntities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EZY_Project.Controllers
{
    public class BillingController : BaseController
    {
        // GET: Billing 
        BillingRepository billingRepo;
        public BillingController()
        {
            billingRepo = new BillingRepository(new OMSEntities());
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetCustomersviaDatatable()
        {

            JsonResult result = new JsonResult();
            if (CurrentUser.user.RoleID == 1)
            {
                var allcustomer = billingRepo.GetAllCustomers();
                var abc = JsonConvert.SerializeObject(allcustomer, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                result = Json(new { data = abc }, JsonRequestBehavior.AllowGet);
                result.MaxJsonLength = int.MaxValue;
            }
            else
            {
                var allcustomer = billingRepo.GetCustomers(CurrentUser.user.ID);
                var abc = JsonConvert.SerializeObject(allcustomer, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                result = Json(new { data = abc }, JsonRequestBehavior.AllowGet);
                result.MaxJsonLength = int.MaxValue;
            }
            return result;
        }
    }
}