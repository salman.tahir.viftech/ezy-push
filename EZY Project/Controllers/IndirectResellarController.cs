﻿using BAL.Repositories;

using Newtonsoft.Json;
using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EZY_Project.Controllers
{
    public class IndirectResellarController : BaseController
    {
        // GET: IndirectResellar
        IndirectResellarRepository indirectRespo;
        public IndirectResellarController()
        {
            indirectRespo = new IndirectResellarRepository(new DAL.DBEntities.OMSEntities());
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetCustomersviaDatatable()
        {

            //GetUsers
            var allcustomer = indirectRespo.GetUsers();
            //var allcustomer = indirectRespo.GetCustomers(CurrentUser.user.ID);
            var abc = JsonConvert.SerializeObject(allcustomer, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });

            var result = Json(new { data = abc }, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
    }
}