﻿using BAL.PCModules;
using DAL.DBEntities;
using Microsoft.Store.PartnerCenter;
using Microsoft.Store.PartnerCenter.Models.Carts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repositories
{
    public class ProductRepository : BaseRepository
    {

        public ProductRepository()
            : base()
        {

        }
        public ProductRepository(OMSEntities ContextDB)
            : base()
        {
            DBContext = ContextDB;
        }

        public IList<vt_Products> GetProducts(string Category)
        {
            return DBContext.Database.SqlQuery<vt_Products>("Select * from vt_Products where IsActive=1 and ProductType=@ProductType", new SqlParameter("@ProductType", Category)).ToList();
            //return DBContext.vt_Products.AsNoTracking().Where(x => x.IsActive == true && x.ProductType == Category).ToList();
        }

        public IList<vt_ProductDetails> GetProductDetails(string MCID)
        {
            return DBContext.Database.SqlQuery<vt_ProductDetails>("Select * from vt_ProductDetails where MCID=@MCID", new SqlParameter("@MCID", MCID)).ToList();
            //return DBContext.vt_ProductDetails.AsQueryable().Where(x => x.MCID == MCID).ToList();
        }

        public vt_Cart CreateCart(vt_Cart carts)
        {
            //vt_Cart _cart = new vt_Cart();            
            try
            {

                //carts.C_MCID = DBContext.vt_Customers.OrderByDescending(x=>x.ID).Select(x => x.CustomerMCID).FirstOrDefault();//"b4b9484b-aa66-4462-b1fd-adb254d376c5";
                //Cart cart = new Carts().CreateCart(carts);
                //carts.CartMCID = cart.Id;
                //carts.BillingCycle = cart.LineItems.Select(x => x.BillingCycle).FirstOrDefault().ToString();
                //carts.CreationTimeStamp = cart.CreationTimestamp;
                //carts.CurrencyCode = cart.LineItems.Select(x => x.CurrencyCode).FirstOrDefault();
                //carts.IsActive = true;
                //_cart.CartMCID = cart.Id;
                //_cart.C_MCID = "a19114cb-4daf-4252-84e4-4ba1fa2c40ac";
                //_cart.P_MCID = carts.P_MCID;
                //_cart.SKUID = carts.SKUID;
                //_cart.Title = carts.Title;
                //_cart.Quantity = carts.Quantity;
                //_cart.BillingCycle = cart.LineItems.Select(x => x.BillingCycle).FirstOrDefault().ToString();
                //_cart.CreationTimeStamp = cart.CreationTimestamp;


                //DBContext.Entry(carts).State = System.Data.Entity.EntityState.Added;
                //DBContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return carts;
        }

        public List<vt_Cart> CreateCartByCookies(IList<vt_Cart> carts, string UserName, string Password, IAggregatePartner partner)
        {
            List<vt_Cart> cartList = new List<vt_Cart>();
            try
            {
                foreach (var item in carts)
                {
                    //carts.C_MCID = DBContext.vt_Customers.OrderByDescending(x => x.ID).Select(x => x.CustomerMCID).FirstOrDefault();//"b4b9484b-aa66-4462-b1fd-adb254d376c5";
                    Cart cart = new Carts(UserName, Password, partner).CreateCart(item);
                    item.CartMCID = cart.Id;
                    item.BillingCycle = cart.LineItems.Select(x => x.BillingCycle).FirstOrDefault().ToString();
                    item.CreationTimeStamp = cart.CreationTimestamp;
                    item.CurrencyCode = cart.LineItems.Select(x => x.CurrencyCode).FirstOrDefault();
                    if(item.BillingCycle.Replace(" ","").ToLower()== "One Time".Replace(" ", "").ToLower())
                    {
                        item.Duration = DBContext.ExclueAll().vt_ProductDetails.Where(z => z.MCID == item.P_MCID && z.SKUID == item.SKUID).Select(x => x.Duration).FirstOrDefault();
                    }
                    item.IsActive = true;
                    //_cart.CartMCID = cart.Id;
                    //_cart.C_MCID = "a19114cb-4daf-4252-84e4-4ba1fa2c40ac";
                    //_cart.P_MCID = carts.P_MCID;
                    //_cart.SKUID = carts.SKUID;
                    //_cart.Title = carts.Title;
                    //_cart.Quantity = carts.Quantity;
                    //_cart.BillingCycle = cart.LineItems.Select(x => x.BillingCycle).FirstOrDefault().ToString();
                    //_cart.CreationTimeStamp = cart.CreationTimestamp;
                    DBContext.Entry(item).State = System.Data.Entity.EntityState.Added;
                    cartList.Add(item);
                }               
                DBContext.SaveChanges();
                return cartList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return carts;
        }

        public IList<vt_Cart> getList(int[] CartIds)
        {
            return DBContext.vt_Cart.Include("vt_Products.vt_ProductDetails").Where(x => CartIds.Contains(x.ID) && x.IsActive == true).ToList();
        }

        public IList<vt_Cart> getList(int CustomerID)
        {
            return DBContext.vt_Cart.Where(x => x.CustomerID == CustomerID && x.IsActive == true).ToList();
        }

        public void Updatecart(vt_Cart cart)
        {
            var result = DBContext.vt_Cart.Where(x => x.ID == cart.ID).FirstOrDefault();
            if (result != null)
            {
                result.Quantity = cart.Quantity;
                result.BillingCycle = cart.BillingCycle;
                DBContext.vt_Cart.Attach(result);
                DBContext.Entry(result).State = EntityState.Modified;
                DBContext.SaveChanges();
            }
        }

        public void RemoveFromCart(string P_MCID, string C_MCID)
        {
            var result = DBContext.ExclueAll().vt_Cart.Where(x => x.P_MCID == P_MCID && x.C_MCID == C_MCID).FirstOrDefault();
            if (result != null)
            {
                result.IsActive = false;
                DBContext.vt_Cart.Attach(result);
                DBContext.Entry(result).State = EntityState.Modified;
                DBContext.SaveChanges();
            }
        }

        public void CancelCart(string C_MCID, DateTime LoginTimeStamp)
        {
            var result = DBContext.Database.ExecuteSqlCommand("Update vt_Cart set IsActive=0 where Convert(varchar,CreationTimeStamp,20)>='" + LoginTimeStamp.ToString("yyyy-MM-dd HH:m:ss") + "' and C_MCID='"+ C_MCID + "'");
            //var result = DBContext.ExclueAll().vt_Cart.Where(x => x.CreationTimeStamp.Value.ToString("yyyy-MM-dd HH:m:ss") == LoginTimeStamp.ToString("yyyy-MM-dd HH:m:ss") && x.C_MCID == C_MCID).FirstOrDefault();
            //if (result != null)
            //{
            //    result.IsActive = false;
            //    DBContext.vt_Cart.Attach(result);
            //    DBContext.Entry(result).State = EntityState.Modified;
            //    DBContext.SaveChanges();
            //}
        }
    }
}
