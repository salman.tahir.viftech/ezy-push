﻿using BAL.Repositories;
using DAL.DBEntities;
using Partner_Center.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EZY_Project.Controllers
{
    public class LoginController : Controller
    {
        AuthenticationRepository AuthenticationRepo;
        public static string LoginMsg;
        public LoginController()
        {
            LoginMsg = string.Empty;
            AuthenticationRepo = new AuthenticationRepository(new OMSEntities());
        }
        // GET: Login
        public ActionResult Index()
        {
            ViewBag.LoginMsg = TempData["Msg"];
            return View();
        }

        public async Task<ActionResult> Login(vt_User _user)
        {
            OMSSession session = new OMSSession();
            string ActionName = string.Empty;
            string ControllerName = string.Empty;
            bool status = false;
            try
            {
                session = AuthenticationRepo.AuthenticateUser(_user);
                if (session != null)
                {
                    status = true;
                    session.LoginTimeStamp = DateTime.UtcNow;
                    Session.Add("OMSSession", session);
                    ActionName = "Index";
                    ControllerName = "Customer";
                    //session.User = new AADAuthentication().UserPartnerOperation;
                }
                //else
                //{
                //    TempData["Msg"] = "Invalid Credentials";
                //    Session.Add("OMSSession", null);
                //    ActionName = "Index";
                //    ControllerName = "Login";
                //}
            }
            catch (Exception ex)
            {
                status = false;
                //throw ex;
            }
            if(!status)
            {
                TempData["Msg"] = "Invalid Credentials";
                Session.Add("OMSSession", null);
                ActionName = "Index";
                ControllerName = "Login";
            }
            return RedirectToAction(ActionName, ControllerName);
        }

        public void DeleteCookies()
        {
            if (System.Web.HttpContext.Current.Request.Cookies["Cart"] != null)
            {
                HttpCookie currentUserCookie = System.Web.HttpContext.Current.Request.Cookies["Cart"];
                if (currentUserCookie != null)
                {
                    System.Web.HttpContext.Current.Response.Cookies.Remove("Cart");
                    System.Web.HttpContext.Current.Request.Cookies.Remove("Cart");
                    //currentUserCookie.Expires = DateTime.Now.AddDays(-10);
                    //currentUserCookie.Value = null;
                    //System.Web.HttpContext.Current.Response.SetCookie(currentUserCookie);
                    //Request.Cookies.Clear();
                }

            }

        }

        public ActionResult Logout()
        {
            if (System.Web.HttpContext.Current.Session["OMSSession"] != null)
            {
                Session.Add("OMSSession", null);
            }
            Session.Abandon();
            //Response.Redirect("Index");
            return RedirectToAction("Index", "Login");
        }

        public void ExpireSession()
        {
            Session.Abandon();
            //Response.Redirect("Logout");
        }
    }
}