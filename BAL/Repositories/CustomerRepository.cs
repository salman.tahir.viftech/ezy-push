﻿using BAL.PCModules;
using DAL;
using DAL.DBEntities;
using Microsoft.Store.PartnerCenter.Models;
using Microsoft.Store.PartnerCenter.Models.Agreements;
using Microsoft.Store.PartnerCenter.Models.Customers;
using Partner_Center.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repositories
{
    public class CustomerRepository : BaseRepository
    {
        public CustomerRepository()
            : base()
        {

        }

        public CustomerRepository(OMSEntities ContextDB)
            : base()
        {
            DBContext = ContextDB;
        }

        public IList<vt_Country> getCountry()
        {
            return DBContext.vt_Country.ToList();
        }

        public Response CheckDomain(string DomainPrefix, string UserName, string Password)
        {
            Response resp = new Response();
            var partnerOperations = new AADAuthentication(UserName, Password).UserPartnerOperation;
            bool isDomainAvailable = !partnerOperations.Domains.ByDomain(DomainPrefix + ".onmicrosoft.com").Exists();
            if (isDomainAvailable)
            {
                resp.DomainPrefix = "This domain prefix is available!";
                resp.status = true;
            }
            else
            {
                resp.DomainPrefix = "This domain prefix is unavailable.";
                resp.status = true;
            }
            return resp;
        }

        public bool InsertCustomer(vt_Customers customer, string UserName, string Password)
        {
            try
            {
                vt_Customers _customer = new vt_Customers();
                vt_CustomerAgreement agree = new vt_CustomerAgreement();
                Microsoft.Store.PartnerCenter.Models.Customers.Customer MicrosoftCustomer = new CustomerModule(UserName, Password).CreateCustomers(customer);
                customer.CustomerMCID = MicrosoftCustomer.Id;
                customer.Company = customer.Company + "A";
                customer.Relationship = MicrosoftCustomer.RelationshipToPartner.ToString();
                customer.Created = DateTime.UtcNow;
                customer.CommerceId = MicrosoftCustomer.CommerceId;
                //MicrosoftAgreement _agree = initializeAgreement(customer);
                //_agree.CustomerMCID = MicrosoftCustomer.Id;
                //_agree.UserMCID = customer.UserID;
                //_agree.FirstName = MicrosoftCustomer.BillingProfile.DefaultAddress.FirstName;
                //_agree.LastName = MicrosoftCustomer.BillingProfile.DefaultAddress.LastName;
                //_agree.Email = MicrosoftCustomer.BillingProfile.Email;
                //_agree.Phone = MicrosoftCustomer.BillingProfile.DefaultAddress.PhoneNumber;
                Agreement _agreement = new Agreements(UserName, Password).CreateAgreement(initializeAgreement(customer));
                agree.CustomerId = customer.ID;
                agree.C_MCID = customer.CustomerMCID;
                agree.TemplateId = _agreement.TemplateId;
                agree.User_MCID = _agreement.UserId;
                agree.DateAgreed = _agreement.DateAgreed;
                agree.Type = AgreementType.MicrosoftCloudAgreement.ToString();
                //DBContext.vt_Customers.Add(customer);

                DBContext.Entry(customer).State = System.Data.Entity.EntityState.Added;
                DBContext.Entry(agree).State = System.Data.Entity.EntityState.Added;
                DBContext.SaveChanges();

                return true;
            }

            catch (Exception ex)
            {
                return false;
            }
        }

        public IList<vt_Customers> GetCustomers()
        {
            //SeekBasedResourceCollection<Microsoft.Store.PartnerCenter.Models.Customers.Customer> Customer = new CustomerModule().GetCustomers();
            //foreach (var item in Customer)
            //{

            //}
            return DBContext.ExclueAll().vt_Customers.ToList();
        }
        public InvoiceStructure GetCustomerById(int id)
        {
            InvoiceStructure invoice = new InvoiceStructure();
            invoice._Customer = new vt_Customers();
            invoice._Invoice = new List<vt_Invoice>();
            invoice._Invoiceazure = new List<vt_InvoiceAzure>();
            invoice._Invoiceoffice = new List<vt_InvoiceOffice>();
            invoice._Customer = DBContext.vt_Customers.Where(x => x.ID == id).FirstOrDefault();
            if (invoice._Customer != null)
            {
                invoice._Invoice = DBContext.vt_Invoice.Where(x => x.CustomerId == invoice._Customer.CustomerMCID).ToList();
                invoice._Invoiceazure = DBContext.vt_InvoiceAzure.Where(x => x.CustomerId == invoice._Customer.CustomerMCID).ToList();
                invoice._Invoiceoffice = DBContext.vt_InvoiceOffice.Where(x => x.CustomerId == invoice._Customer.CustomerMCID).ToList();
            }
            return invoice;
        }

        public IList<vt_Customers> GetCustomers(int id)
        {
            //SeekBasedResourceCollection<Microsoft.Store.PartnerCenter.Models.Customers.Customer> Customer = new CustomerModule().GetCustomers();
            //foreach (var item in Customer)
            //{

            //}
            return DBContext.ExclueAll().vt_Customers.Where(x => x.UserId == id && x.IsdirectResellar == false).ToList();
        }


        //public SeekBasedResourceCollection<Microsoft.Store.PartnerCenter.Models.Customers.Customer> GetCustomersByPartnerCenter()
        //{
        //    //SeekBasedResourceCollection<Microsoft.Store.PartnerCenter.Models.Customers.Customer> Customer = new CustomerModule().GetCustomers();

        //    return new CustomerModule().GetCustomers();
        //}


        public MicrosoftAgreement initializeAgreement(vt_Customers customer)
        {
            MicrosoftAgreement _agree = new MicrosoftAgreement();
            _agree.CustomerMCID = customer.CustomerMCID;
            _agree.TempleteMCID = DBContext.vt_MicrosoftAgreement.Select(x => x.TemplateId).FirstOrDefault();
            _agree.UserMCID = customer.UserID;
            _agree.FirstName = customer.PrimaryFirstName;
            _agree.LastName = customer.PrimaryLastName;
            _agree.Email = customer.PrimaryEmail;
            _agree.Phone = customer.PrimaryPhone;
            return _agree;
        }

    }
}
