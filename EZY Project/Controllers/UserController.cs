﻿using BAL.Repositories;
using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EZY_Project.Controllers
{
    public class UserController : BaseController
    {

        UserRepository userrepo;

        public UserController()
        {
            userrepo = new UserRepository(new DAL.DBEntities.OMSEntities());
        }
        // GET: User
        public ActionResult Index()
        {
            return View(userrepo.Getuser());
        }

        [HttpGet]
        public ActionResult Create(string CustomerId)
        {
            var sess = CurrentUser;

            vt_User user = userrepo.GetuserbyId(Convert.ToInt32(CustomerId));

            try
            {


                List<SelectListItem> AllRegions = new List<SelectListItem>();
                SelectListItem ListItem = new SelectListItem();
                ListItem.Text = "--Select Region--";
                ListItem.Value = "";
                AllRegions.Add(ListItem);
                foreach (var item in userrepo.GetAllRegion())
                {
                    SelectListItem SelectListItem = new SelectListItem();
                    SelectListItem.Text = item.Region;
                    SelectListItem.Value = item.ID.ToString();
                    AllRegions.Add(SelectListItem);


                }

                List<SelectListItem> AllRoles = new List<SelectListItem>();
                ListItem = new SelectListItem();
                ListItem.Text = "--Select Role--";
                ListItem.Value = "";
                AllRoles.Add(ListItem);
                foreach (var item in userrepo.GetAllRole())
                {
                    SelectListItem SelectListItem = new SelectListItem();
                    SelectListItem.Text = item.Role;
                    SelectListItem.Value = item.ID.ToString();
                    AllRoles.Add(SelectListItem);

                }

                ViewBag.AllRegions = AllRegions;
                ViewBag.AllRoles = AllRoles;
            }
            catch (Exception ex)
            {

            }


            return PartialView("_Save", user);
        }

        [HttpPost]
        public ActionResult CreateUser(vt_User user)
        {
            var sess = CurrentUser;
            string msg = string.Empty;
            try
            {
                //if (ModelState.IsValid)
                //{
                    if (user.ID > 0)
                    {
                        userrepo.updateUserbyId(user);
                        msg = "Update User Successfully";
                        //if (!userrepo.CheckUserName(user.Email, user.CompanyId))
                        //{
                        //    userrepo.UpdateUser(user, sess.vt_user.id);
                        //}
                        //else
                        //{
                        //    var a = userrepo.GetUserById(user.id);
                        //    ViewBag.UserName = a.Email;
                        //    ViewBag.Company = userrepo.GetCompanys(sess.vt_user.CompanyId);
                        //    ViewBag.Title = "UserName Already Exist";
                        //    return PartialView("_Save", user);
                        //}
                    }
                    else
                    {
                        userrepo.InsertUser(user);
                        msg = "Insert User Successfully";
                        //if (userrepo.CheckUserName(user.Email, user.CompanyId))
                        //{
                        //    ViewBag.Company = userrepo.GetCompanys(sess.vt_user.CompanyId);
                        //    ViewBag.Title = "UserName Already Exist";
                        //    return PartialView("_Save", user);
                        //}
                        //else
                        //{
                        //    userrepo.InsertUser(user, sess.vt_user.id);
                        //}
                    }
                    List<SelectListItem> AllRegions = new List<SelectListItem>();
                    SelectListItem ListItem = new SelectListItem();
                    ListItem.Text = "--Select Region--";
                    ListItem.Value = "";
                    AllRegions.Add(ListItem);
                    foreach (var item in userrepo.GetAllRegion())
                    {
                        SelectListItem SelectListItem = new SelectListItem();
                        SelectListItem.Text = item.Region;
                        SelectListItem.Value = item.ID.ToString();
                        AllRegions.Add(SelectListItem);


                    }

                    List<SelectListItem> AllRoles = new List<SelectListItem>();
                    ListItem = new SelectListItem();
                    ListItem.Text = "--Select Role--";
                    ListItem.Value = "";
                    AllRoles.Add(ListItem);
                    foreach (var item in userrepo.GetAllRole())
                    {
                        SelectListItem SelectListItem = new SelectListItem();
                        SelectListItem.Text = item.Role;
                        SelectListItem.Value = item.ID.ToString();
                        AllRoles.Add(SelectListItem);

                    }

                    ViewBag.AllRegions = AllRegions;
                    ViewBag.AllRoles = AllRoles;
                    //return RedirectToAction("Index");
                    return Json(new { status=true,message= msg }, JsonRequestBehavior.AllowGet);
                //}

            }
            catch (Exception ex)
            {

            }
            return PartialView("_Save", user);

            //else
            //{
            //    ViewBag.Company = userrepo.GetCompanys(sess.vt_user.CompanyId);
            //    ViewBag.Title = "Fill All Required Fields";
            //    return PartialView("_Save", user);
            //}


        }

        public JsonResult DeleteUser(int id)
        {
            bool Status = false;
            try
            {

                if (userrepo.DeleteUserbyId(id) == true)
                {
                    Status = true;
                    //userrepo.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Status = false;
            }

            return Json(new { success = Status }, JsonRequestBehavior.AllowGet);
        }
    }
}