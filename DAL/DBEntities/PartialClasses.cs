﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DAL.DBEntities
{
    [MetadataType(typeof(vt_CustomersMetaData))]
    public partial class vt_Customers
    {
        //[Remote("IsDomainExist", "Customer", ErrorMessage = "Email Already Exist.")]
        //public string Account { get; set; }

        public string UserID { get; set; }
    }
}
