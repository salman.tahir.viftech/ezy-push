﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using DAL.DBEntities;

namespace SDViewModel.Model
{
    public class vt_Common
    {
        public enum Roles
        {
            Admin = 1,
            Supervisor = 2,
            Instructor = 3,
            User = 4
        }


        public static string DecryptPassword(string Password)
        {
            return Decrypt(Password);
        }

        public static string EncryptPassword(string Password)
        {
            return Encrypt(Password);
        }
        public enum Provider
        {
            linkedin,
            Facebook,
            Portal
        }

        public static string Encrypt(string originalString)
        {
            return Encrypt(originalString, getKey);
        }

        public static byte[] getKey
        {
            get
            {
                return ASCIIEncoding.ASCII.GetBytes(ConfigurationManager.AppSettings["EncryptKey"].ToString());
            }
        }
        public static string Encrypt(string originalString, byte[] bytes)
        {
            try
            {
                if (String.IsNullOrEmpty(originalString))
                {
                    throw new ArgumentNullException("The string which needs to be encrypted can not be null.");
                }

                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                MemoryStream memoryStream = new MemoryStream();
                CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateEncryptor(bytes, bytes), CryptoStreamMode.Write);

                StreamWriter writer = new StreamWriter(cryptoStream);
                writer.Write(originalString);
                writer.Flush();
                cryptoStream.FlushFinalBlock();
                writer.Flush();


                //return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
                var Converting = Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
                Converting = Regex.Replace(Converting, "/", "-");
                Converting = Regex.Replace(Converting, "[+]", "_");
                return Converting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string Decrypt(string originalString)
        {
            return Decrypt(originalString, getKey);
        }

        public static string Decrypt(string cryptedString, byte[] bytes)
        {
            try
            {
                if (String.IsNullOrEmpty(cryptedString))
                {
                    throw new ArgumentNullException("The string which needs to be decrypted can not be null.");
                }
                cryptedString = Regex.Replace(cryptedString, "-", "/");
                cryptedString = Regex.Replace(cryptedString, "[ ]", "+");
                cryptedString = Regex.Replace(cryptedString, "_", "+");

                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(cryptedString));
                CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateDecryptor(bytes, bytes), CryptoStreamMode.Read);
                StreamReader reader = new StreamReader(cryptoStream);

                return reader.ReadToEnd();
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static int ToInt(object value)
        {
            int parseVal;
            return ((value == null) || (value == DBNull.Value)) ? 0 : int.TryParse(value.ToString(), out parseVal) ? parseVal : 0;
        }      

        //public static OMSSession CreateSession(UserValidateListResposne Response)
        //{
        //    OMSSession User = new OMSSession();
        //    User.SessionUser.ID = Response.objects.ID;
        //    User.SessionUser.RoleID = Response.objects.RoleID;
        //    User.SessionUser.Email = Response.objects.Email;
        //    User.SessionUser.Image = Response.objects.Image;
        //    User.SessionUser.Latitude = Response.objects.Latitude;
        //    User.SessionUser.Longitude = Response.objects.Longitude;
        //    User.SessionUser.Provider = Response.objects.Provider;
        //    User.SessionUser.UserName = Response.objects.UserName;
        //    User.SessionUser.FirstName = Response.objects.FirstName;
        //    User.SessionUser.LastName = Response.objects.LastName;
        //    //User.SessionUser = Response.objects;
        //    return User;
        //}

        //public static SDSession SessionSD(vt_UserProfile user)
        //{
        //    SDSession sess = new SDSession();
        //    string strResponse = CreateRequest(ConfigurationManager.AppSettings["APIHostDomain"] + "/api/Authentication/ValidateUser/", user);
        //    UserValidateListResposne Response = JsonConvert.DeserializeObject<UserValidateListResposne>(strResponse);
        //    var list = Response.objects;
        //    list.Password = !string.IsNullOrEmpty(list.Password) ? vt_Common.Encrypt(list.Password) : list.Password;
        //    list.Email = vt_Common.Encrypt(list.Email);
        //    sess.SessionUser = list;
        //    //sess.SessionUser.ID = Response.objects.ID;
        //    //sess.SessionUser.RoleID = Response.objects.RoleID;
        //    //sess.SessionUser.Email = Response.objects.Email;
        //    //sess.SessionUser.Image = Response.objects.Image;
        //    //sess.SessionUser.Latitude = Response.objects.Latitude;
        //    //sess.SessionUser.Longitude = Response.objects.Longitude;
        //    //sess.SessionUser.Provider = Response.objects.Provider;
        //    //sess.SessionUser.UserName = Response.objects.UserName;
        //    //sess.SessionUser.FirstName = Response.objects.FirstName;
        //    //sess.SessionUser.LastName = Response.objects.LastName;
        //    //sess.SessionUser =vt_Common.CreateSession(Response);

        //    return sess;
        //}

        public static string SendAttachmentEmail(string UserName, string Email, string Link,string type)
        {
            try
            {
                //MailTo = ConfigurationSettings.AppSettings["MailTo"].ToString();

                string to = "", cc = "", bcc = "", subject = "", ReplyTo = "", attachment = "";

                to = Email;
                if (ConfigurationSettings.AppSettings["cc"].ToString().Trim() != "")
                {
                    cc = ConfigurationSettings.AppSettings["cc"].ToString();
                }
                if (ConfigurationSettings.AppSettings["bcc"].ToString().Trim() != "")
                {
                    bcc = ConfigurationSettings.AppSettings["bcc"].ToString();
                }

                /*tring[] ToEmails = to.Split(',');*/
                using (MailMessage message = new MailMessage())
                {
                    string[] Add = to.ToString().Split(',');
                    message.From = new MailAddress(ConfigurationSettings.AppSettings["MailFrom"].ToString(), ConfigurationSettings.AppSettings["MailFromName"].ToString());
                    if (Add.Length > 1)
                    {
                        for (int a = 0; a < Add.Length; a++)
                        {
                            message.To.Add(new MailAddress(Add[a].ToString()));
                        }
                    }
                    else { message.To.Add(new MailAddress(to)); }
                    if (cc != null && cc != "")
                    {
                        message.CC.Add(new MailAddress(cc));
                    }
                    if (bcc != null && bcc != "")
                    {
                        message.Bcc.Add(new MailAddress(bcc));
                    }
                    if (ReplyTo != null && ReplyTo != "")
                    {
                        message.ReplyToList.Add(new MailAddress(ReplyTo));
                    }
                    message.IsBodyHtml = true;
                
                    if(type== "Forgot Password")
                    {
                        message.Subject = "Reset Password";
                        message.Body = @"<!DOCTYPE html>
<html>
<head>
    <title></title>

    <style>
        table {
            border-collapse: collapse;
            border: 0;
        }
    </style>
</head>
<body>
    <div style='width: 600px; margin: 0 auto; padding: 15px;'>
        <table style='width: 100%;'>
            <tr>
                <td align='center' style='background-color: #f2f5f9; padding: 15px;'>
                    <img src='https://superdrive.viftechuat.com/Content/SDImage/logo.png' width='200px'>
                    <h1 style='font-weight: 500;'>Welcome to SuperDrive<br/>
                        <span style='color: #000000; font-size: 36px;'>" + UserName + @"</span><br/>
                        <span style='width: 100px; height: 2px; background-color: #333; display: inline-block; margin-top: 4px;'></span>
                    </h1>
                </td>
            </tr>
            <tr>
                <td align='center' style='background-image: url(https://superdrive.viftechuat.com/Content/SDImage/img-main-bg.jpg); padding: 15px'>
                    <h4 style='font-size: 30px; color: #fff; margin-bottom: 15px; font-weight: 500;'>Change Password <br/>
<span style='width: 100px; height: 2px; background-color: #fff; display: inline-block; margin-top: 4px;'>
                    </span>
                    </h4>
                    <p style='font-size: 17px; line-height: 30px; text-align: justify; color: #fff; padding: 10px 40px;'>
                      " + Link + @"
                    </p>
                </td>

            </tr>
        </table>
        <table width='100 % '>
            <tr>
                <td style='height: 20px;'></td>
            </tr>

        </table>
    </div>
</body>
</html>";
                    }
                    if(type== "Set Password")
                    {
                        message.Subject = "Set Password";
                        message.Body = @"<!DOCTYPE html>
<html>
<head>
    <title></title>

    <style>
        table {
            border-collapse: collapse;
            border: 0;
        }
    </style>
</head>
<body>
    <div style='width: 600px; margin: 0 auto; padding: 15px;'>
        <table style='width: 100%;'>
            <tr>
                <td align='center' style='background-color: #f2f5f9; padding: 15px;'>
                    <img src='https://superdrive.viftechuat.com/Content/SDImage/logo.png' width='200px'>
                    <h1 style='font-weight: 500;'>Welcome to SuperDrive<br/>
                        <span style='color: #000000; font-size: 36px;'>" + UserName + @"</span><br/>
                        <span style='width: 100px; height: 2px; background-color: #333; display: inline-block; margin-top: 4px;'></span>
                    </h1>
                </td>
            </tr>
            <tr>
                <td align='center' style='background-image: url(https://superdrive.viftechuat.com/Content/SDImage/img-main-bg.jpg); padding: 15px'>
                    <h4 style='font-size: 30px; color: #fff; margin-bottom: 15px; font-weight: 500;'>Set Your Password From This Link <br/>
<span style='width: 100px; height: 2px; background-color: #fff; display: inline-block; margin-top: 4px;'>
                    </span>
                    </h4>
                    <p style='font-size: 17px; line-height: 30px; text-align: justify; color: #fff; padding: 10px 40px;'>
                      " + Link + @"
                    </p>
                </td>

            </tr>
        </table>
        <table width='100 % '>
            <tr>
                <td style='height: 20px;'></td>
            </tr>

        </table>
    </div>
</body>
</html>";
                    }
                 
                    //message.Body = Link;


                    if (!string.IsNullOrEmpty(attachment))
                    {
                        System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(attachment);
                        message.Attachments.Add(attach);
                    }
                    using (SmtpClient smtp = new SmtpClient())
                    {
                        smtp.UseDefaultCredentials = true;
                        smtp.Host = ConfigurationSettings.AppSettings["SmtpServer"].ToString();


                        if (bool.Parse(ConfigurationSettings.AppSettings["UseCredentials"]))
                        {
                            string EmailPass = (ConfigurationSettings.AppSettings["Password"].ToString());
                            smtp.Credentials = new NetworkCredential(ConfigurationSettings.AppSettings["UserEmail"].ToString(), EmailPass);

                        }
                        else
                        {
                            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                            smtp.UseDefaultCredentials = false;

                        }
                        smtp.Port = Convert.ToInt32(ConfigurationSettings.AppSettings["Port"].ToString());
                        smtp.EnableSsl = Convert.ToBoolean(ConfigurationSettings.AppSettings["isSSL"].ToString());


                        try
                        {
                            smtp.Send(message);
                        }
                        catch (Exception ex)
                        {
                            return "Exception: " + ex.Message.ToString();
                        }
                    }
                    if (type == "Forgot Password")
                        return "Successfully email send for reset pass";
                    if (type == "Set Password")
                        return "Successfully email send for set pass";
                    else
                        return "";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public static IEnumerable<string> BindCountryDropDown()
        {
            RegionInfo country = new RegionInfo(new CultureInfo("en-US", false).LCID);

            List<string> countryNames = new List<string>();

            //To get the Country Names from the CultureInfo installed in windows
            foreach (CultureInfo cul in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                country = new RegionInfo(new CultureInfo(cul.Name, false).LCID);
                countryNames.Add(country.DisplayName.ToString());
            }
            //Assigning all Country names to IEnumerable
            //ViewBag.CountryName = countryNames.OrderBy(names => names).Distinct().ToList();
            IEnumerable<string> nameAdded = countryNames.OrderBy(names => names).Distinct();
            return nameAdded.ToList();


        }

        public static DateTime GetCurrentDateTime()
        {

            DateTime currentTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["Current_Date_Time"].ToString()));
            return currentTime;
        }

        public static DateTime ConvertDateString(string Date)
        {
            string[] formats = {"M/d/yyyy h:mm:ss tt",
                           "M/d/yyyy h:mm tt",
                         "MM/dd/yyyy hh:mm:ss",
                          "M/d/yyyy h:mm:ss",
                         "M/d/yyyy hh:mm tt",
                        "M/d/yyyy hh tt",
                         "M/d/yyyy h:mm",
                         "M/d/yyyy h:mm",
                         "MM/dd/yyyy hh:mm",
                         "M/dd/yyyy hh:mm",
                         "MM/d/yyyy HH:mm:ss.ffffff",
                          "d/MM/yyyy",
                           "MM/d/yyyy",
                         "d/M/yyyy h:mm:ss tt",
                         "d/M/yyyy h:mm tt",
                         "dd/MM/yyyy hh:mm:ss",
                         "d/M/yyyy h:mm:ss",
                         "d/M/yyyy hh:mm tt",
                         "d/M/yyyy hh tt",
                         "d/M/yyyy h:mm",
                         "d/M/yyyy h:mm",
                         "dd/MM/yyyy hh:mm",
                         "dd/M/yyyy hh:mm",
                         "d/MM/yyyy HH:mm:ss.ffffff"};


            return  DateTime.ParseExact(Date, formats,new CultureInfo("en-US"),DateTimeStyles.None); 
        }


        public static string SendEmail(string UserName, string ToEmail, string body, string type, string cc)
        {
            try
            {
                //MailTo = ConfigurationSettings.AppSettings["MailTo"].ToString();

                string to = "",  bcc = "", subject = "", ReplyTo = "", attachment = "";

                to = ToEmail;
                //if (ConfigurationSettings.AppSettings["cc"].ToString().Trim() != "")
                //{
                //    cc = ConfigurationSettings.AppSettings["cc"].ToString();
                //}
                //if (ConfigurationSettings.AppSettings["bcc"].ToString().Trim() != "")
                //{
                //    bcc = ConfigurationSettings.AppSettings["bcc"].ToString();
                //}

                //string[] ToEmails = to.Split(',');
                using (MailMessage message = new MailMessage())
                {
                    string[] Add = to.ToString().Split(',');
                    message.From = new MailAddress(ConfigurationSettings.AppSettings["MailFrom"].ToString(), ConfigurationSettings.AppSettings["MailFromName"].ToString());
                    if (Add.Length > 1)
                    {
                        for (int a = 0; a < Add.Length; a++)
                        {
                            message.To.Add(new MailAddress(Add[a].ToString()));
                        }
                    }
                    else { message.To.Add(new MailAddress(to)); }
                    if (cc != null && cc != "")
                    {
                        message.CC.Add(new MailAddress(cc));
                    }
                    if (bcc != null && bcc != "")
                    {
                        message.Bcc.Add(new MailAddress(bcc));
                    }
                    if (ReplyTo != null && ReplyTo != "")
                    {
                        message.ReplyToList.Add(new MailAddress(ReplyTo));
                    }
                    message.IsBodyHtml = true;


                    message.Subject = type;
                    message.Body = body;



                    using (SmtpClient smtp = new SmtpClient())
                    {
                        smtp.UseDefaultCredentials = true;
                        smtp.Host = ConfigurationSettings.AppSettings["SmtpServer"].ToString();


                        if (bool.Parse(ConfigurationSettings.AppSettings["UseCredentials"]))
                        {
                            string EmailPass = (ConfigurationSettings.AppSettings["Password"].ToString());
                            smtp.Credentials = new NetworkCredential(ConfigurationSettings.AppSettings["UserEmail"].ToString(), EmailPass);

                        }
                        else
                        {
                            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                            smtp.UseDefaultCredentials = false;

                        }
                        smtp.Port = Convert.ToInt32(ConfigurationSettings.AppSettings["Port"].ToString());
                        smtp.EnableSsl = Convert.ToBoolean(ConfigurationSettings.AppSettings["isSSL"].ToString());


                        try
                        {
                            smtp.Send(message);
                        }
                        catch (Exception ex)
                        {
                            return "Exception: " + ex.Message.ToString();
                        }
                    }
                    //if (type == "Forgot Password")
                    //    return "Successfully email send for reset pass";
                    //if (type == "Set Password")
                    //    return "Successfully email send for set pass";
                    //else
                    return "email sent successfully";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        public static TimeSpan Subtract30MinutesFromTime()
        {
            DateTime Time_30_Minutes;
            DateTime.TryParseExact("00:30:00", "HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out Time_30_Minutes);
            return Time_30_Minutes.TimeOfDay;
        }
        public static TimeSpan ConvertTimeString(double hour, double minute, double second)
        {
            string strHour = "00";
            string strMinute = "00";
            string strSecond = "00";

            if (hour < 10)
                strHour = "0" + hour.ToString();
            else
                strHour = hour.ToString();

            if (minute < 10)
                strMinute = "0" + minute.ToString();
            else
                strMinute = minute.ToString();

            if (second < 10)
                strSecond = "0" + second.ToString();
            else
                strSecond = second.ToString();

            string strTotalTime = strHour + ":" + strMinute + ":" + strSecond;

          

          return  ConvertStringTime(strTotalTime);
        }

     public static TimeSpan ConvertStringTime(string Time)
        {
            DateTime dt_TotalTime;
            DateTime.TryParseExact(Time, "HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt_TotalTime);
            return dt_TotalTime.TimeOfDay;
        }

        public static TimeSpan GetCurrentTime()
        {
            string Strcurrenttime = DateTime.Now.ToString("HH:mm:ss");
            DateTime dt_TotalTime;
            DateTime.TryParseExact(Strcurrenttime, "HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt_TotalTime);
            return dt_TotalTime.TimeOfDay;
        }     

        public static int CalculateAge(DateTime dOB)
        {
            int age = 0;
            age = DateTime.Now.Year - dOB.Year;
            if (DateTime.Now.DayOfYear < dOB.DayOfYear)
                age = age - 1;

            return age;
        }

        #region GetDistance_From_2_Lat_Lng
        public static double GetDistance(double lat1, double lon1, double lat2, double lon2, string unit)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == "K")
            {
                dist = dist * 1.609344;
            }
            else if (unit == "N")
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        public static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        public static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }

        #endregion
    }
}
