﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModal
{
   public class Customers
    {
        public int ID { get; set; }
        public string Company { get; set; }
        public string CountryID { get; set; }
        public string CustomerMCID { get; set; }
        public string ResellerMPN_ID { get; set; }
        public string CompanyAccount { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State_Province { get; set; }
        public string Zip { get; set; }
        public string PrimaryFirstName { get; set; }
        public string PrimaryLastName { get; set; }
        public string PrimaryEmail { get; set; }
        public string PrimaryPhone { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
    }
}
