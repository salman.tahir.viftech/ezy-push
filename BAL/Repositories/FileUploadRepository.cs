﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repositories
{
    public class FileUploadRepository:BaseRepository
    {
        public FileUploadRepository()
            :base()
        {

        }

        public FileUploadRepository(OMSEntities ContextBD)
            :base()
        {
            DBContext = ContextBD;
        }

        public IList<vt_Invoice> GetYearMonth()
        {
            return DBContext.vt_Invoice.ToList();
        }

        public int GetInvoiceId(string Type,string Name,int Month,int year)
        {
            vt_Invoice_P _Invoice = new vt_Invoice_P();
            _Invoice.FileName = Name;
            _Invoice.Type = Type;
            _Invoice.Month = Month;
            _Invoice.Year = year;
            
            DBContext.Entry(_Invoice).State = System.Data.Entity.EntityState.Added;
            DBContext.SaveChanges();
            int InoviceId = _Invoice.Id;
            return InoviceId;
        }
        public IList<vt_Invoice_P> GetInvoivesData()
        {
            //SeekBasedResourceCollection<Microsoft.Store.PartnerCenter.Models.Customers.Customer> Customer = new CustomerModule().GetCustomers();
            //foreach (var item in Customer)
            //{

            //}
            return DBContext.vt_Invoice_P.ToList();
        }
    }
}
