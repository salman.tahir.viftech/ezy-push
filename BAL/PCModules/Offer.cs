﻿using Microsoft.Store.PartnerCenter;
using Partner_Center.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EZY_Project.PCModules
{
    public class Offer :AADAuthentication 
    {
        public static IAggregatePartner partnerOperation = null;
        public Offer()
        {
            partnerOperation = this.UserPartnerOperation;
        }


        public void GetOffers(string countryCode)
        {
            var offers = partnerOperation.Offers.ByCountry(countryCode).Get();
        }

        public void GetOfferbyId(string countryCode,string offerId)
        {
            var offer = partnerOperation.Offers.ByCountry(countryCode).ById(offerId).Get();
            var offerAddOns = partnerOperation.Offers.ByCountry(countryCode).ById(offerId).AddOns.Get();
        }

        public void GetCustomerOffer(string customerIdToRetrieve)
        {
            var offers = partnerOperation.Customers.ById(customerIdToRetrieve).Offers.Get();
        }
    }
}