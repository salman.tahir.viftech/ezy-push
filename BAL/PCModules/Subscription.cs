﻿
using Microsoft.Store.PartnerCenter;
using Microsoft.Store.PartnerCenter.Models;
using Microsoft.Store.PartnerCenter.Models.Orders;
using Partner_Center.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EZY_Project.PCModules
{

    public class PCSubscription : AADAuthentication
    {
        public static IAggregatePartner partnerOperation = null;
        public PCSubscription(string UserName, string Password, IAggregatePartner partner)
            : base(UserName, Password)
        {
            if (partner == null)
            { partnerOperation = this.UserPartnerOperation; }
            else
            {
                partnerOperation = partner;
            }
            //partnerOperation = this.UserPartnerOperation;
        }

        public void GetSSubscription(string CustomerID, string SubscriptionId)
        {
            var customerSubscription = partnerOperation.Customers.ById(CustomerID).Subscriptions.ById(SubscriptionId).Get();
        }

        public void GetSubscriptionById(string CustomerID)
        {
            var customerSubscription = partnerOperation.Customers.ById(CustomerID).Subscriptions.Get();
        }

        public ResourceCollection<Microsoft.Store.PartnerCenter.Models.Subscriptions.Subscription> GetSubscriptionByOrderId(string CustomerId, string OrderId)
        {
            return partnerOperation.Customers.ById(CustomerId).Subscriptions.ByOrder(OrderId).Get();
        }
        public void CreateSubscription(string CustomerId, string OfferId, string SubscriptionId)
        {
            var subscriptionOperations = partnerOperation.Customers.ById(CustomerId).Subscriptions.ById(SubscriptionId);

            // get the parent subscription details

            var parentSubscription = subscriptionOperations.Get();

            // in order to buy an add on subscription for this offer, we need to patch/update the order through which the base offer was purchased
            // by creating an order object with a single line item which represents the add-on offer purchase.
            var orderToUpdate = new Order()
            {
                ReferenceCustomerId = CustomerId,
                LineItems = new List<OrderLineItem>()
                {
                    new OrderLineItem()
                    {
                        LineItemNumber = 0,
                        OfferId = OfferId,
                        FriendlyName = "Some friendly name",
                        Quantity = 2,
                        ParentSubscriptionId = SubscriptionId
                    }
                }
            };

            // update the order to apply the add on purchase
            Order updatedOrder = partnerOperation.Customers.ById(CustomerId).Orders.ById(parentSubscription.OrderId).Patch(orderToUpdate);

            // fetch the subscription add ons and display these

            var subscriptionAddOns = subscriptionOperations.AddOns.Get();
        }
    }
}