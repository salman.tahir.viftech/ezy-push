﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Response
    {
        public bool status { get; set; }
        public string DomainPrefix { get; set; }
    }


    public class Product
    {
        public IList<vt_Products> Products { get; set; }
        public IList<vt_ProductDetails> ProductDetail { get; set; }
    }


    public class Cart
    {
        public IList<vt_Cart> Carts { get; set; }

        public vt_Cart cartModel { get; set; }
    }

    public class Order
    {
        public Customer _Customer { get; set; }
        public IList<vt_Cart> Cart { get; set; }
    }

    public class MicrosoftAgreement
    {
        public string CustomerMCID { get; set; }
        public string TempleteMCID { get; set; }
        public string UserMCID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
