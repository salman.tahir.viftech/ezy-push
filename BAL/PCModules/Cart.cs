﻿using DAL.DBEntities;
using Microsoft.Store.PartnerCenter;
using Microsoft.Store.PartnerCenter.Models.Carts;
using Partner_Center.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.PCModules
{
    public class Carts : AADAuthentication
    {
        public static IAggregatePartner partnerOperation = null;
        public Carts(string UserName, string Password, IAggregatePartner partner)
            : base(UserName, Password)
        {
            if (partner == null)
            { partnerOperation = this.UserPartnerOperation; }
            else
            {
                partnerOperation = partner;
            }

        }

        public Cart CreateCart(vt_Cart carts)
        {
            var sku = partnerOperation.Products.ByCountry("PK").ById(carts.P_MCID).Skus.ById(carts.SKUID).Get();
            var availabilities = partnerOperation.Products.ByCountry("PK").ById(carts.P_MCID).Skus.ById(carts.SKUID).Availabilities.Get();
            //if (sku.ProvisioningVariables != null)
            //{
            //    scope = this.ObtainScope("Enter the Scope for the Provisioning status");
            //    subscriptionId = this.ObtainAzureSubscriptionId("Enter the Subscription Id");
            //    duration = (string)sku.DynamicAttributes["duration"];
            //}

            var cart = new Cart()
            {
                LineItems = new List<CartLineItem>()
                {
                    new CartLineItem()
                    {
                        CatalogItemId = sku.SupportedBillingCycles.ToArray().First() !=Microsoft.Store.PartnerCenter.Models.Products.BillingCycleType.OneTime? carts.P_MCID+":"+carts.SKUID:availabilities.Items.First().CatalogItemId,
                        FriendlyName = carts.Title,
                        Quantity =(int) carts.Quantity,
                        BillingCycle = sku.SupportedBillingCycles.ToArray().First(),
                        CurrencyCode="USD"
                        //,
                        //ProvisioningContext = (sku.ProvisioningVariables == null) ? null : new Dictionary<string, string>
                        //{
                        //    { "subscriptionId", subscriptionId },
                        //    { "scope", scope },
                        //    { "duration", duration }
                        //}
                    }
                }
            };
            return partnerOperation.Customers.ById(carts.C_MCID).Carts.Create(cart);

        }


        public Cart UpdateCart(vt_Cart carts)
        {
            //string customerId = this.ObtainCustomerId("Enter the ID of the customer making the purchase");
            //string cartId = this.ObtainCartID("Enter the ID of cart for which changes are to be made");
            //int quantityChange = int.Parse(this.ObtainQuantity("Enter the amount the quantity has to be changed"));

            Cart existingCart = partnerOperation.Customers.ById("").Carts.ById("").Get();

            //this.Context.ConsoleHelper.WriteObject(existingCart, "Cart to be updated");
            //this.Context.ConsoleHelper.StartProgress("Updating cart");

            //existingCart.LineItems.ToArray()[0].Quantity += quantityChange;

            //var updatedCart = partnerOperations.Customers.ById(customerId).Carts.ById(cartId).Put(existingCart);
            return existingCart;
        }
    }
}
